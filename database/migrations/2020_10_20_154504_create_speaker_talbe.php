<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpeakerTalbe extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('speakers', function (Blueprint $table) {
            $table->bigIncrements('id',20);
            $table->uuid('uuid')->index();
            $table->string('name', 36);
            $table->char('name_e', 100)->nullable();
            $table->string('job_title',20)->nullable();
            $table->char('job_title_e',50)->nullable();
            $table->bigInteger('speakers_type')->default(0);
            $table->string('photo',100)->nullable();
            $table->string('introduction',100)->nullable();
            $table->char('introduction_e',100)->nullable();
            $table->text('content')->nullable();
            $table->text('content_e')->nullable();
            $table->text('link_fb')->nullable();
            $table->bigInteger('created_by')->default(0);
            $table->bigInteger('updated_by')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('speakers');
    }
}
