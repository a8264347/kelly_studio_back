<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticleDescTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('article_desc', function (Blueprint $table) {
            $table->bigIncrements('id',20);
            $table->uuid('uuid')->index();
            $table->bigInteger('article_id')->unsigned();
            $table->text('content')->nullable();
            $table->text('content_e')->nullable();
            $table->string('photo',100)->nullable();
            $table->string('photo_desc',20)->nullable();
            $table->unsignedTinyInteger('type')->default(0);
            $table->bigInteger('created_by')->default(0);
            $table->bigInteger('updated_by')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('article_desc');
    }
}
