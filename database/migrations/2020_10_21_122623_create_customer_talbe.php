<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerTalbe extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->bigIncrements('id',20);
            $table->uuid('uuid')->index();
            $table->string('name', 36);
            $table->char('name_e', 100)->nullable();
            $table->string('job_title',20)->nullable();
            $table->char('job_title_e',50)->nullable();
            $table->string('photo',100)->nullable();
            $table->string('introduction',100)->nullable();
            $table->char('introduction_e',100)->nullable();
            $table->text('message')->nullable();
            $table->bigInteger('class_id')->default(0);
            $table->bigInteger('created_by')->default(0);
            $table->bigInteger('updated_by')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
