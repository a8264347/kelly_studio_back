<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::where('email', '=', 'edmond.tong@gmail.com')->first();
        if ($user === null) {
            DB::table('users')->insert([
                'email' => 'edmond.tong@gmail.com',
                'password' => Hash::make(env('USER_DEFAULT_PASSWORD'))
            ]);
        }
    }
}
