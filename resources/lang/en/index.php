<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Index Language Lines
    |--------------------------------------------------------------------------
    |
    |
    |
    */
    'title'           => '後台管理',
    'hello_title'     => 'Popular Beauty',
    'hello_ceo'       => 'Kelly',
    'hello_ceo_move_word' => 'Studio',
    'hello_info'      => 'Kelly will teach you how to get the certificate, Join our Amazing online classes',
    'hello_class_btn' => 'View Classes',
    'classes_topic'   => 'Popular Classes',
    'classes_info'    => 'Make yourself more profession or more beautiful.',
    'hello_all_view_class_btn' => 'View All Classes',
    'article_topic'   => 'Amazing Online Articles About Classes',
    'article_info'    => "It's best only when it fits you. Contact us for more information.",
    'speaker_topic'   => 'Our Instructors',
    'speaker_info'    => 'Every teachers will be full steam ahead!Come on!',
    'blog_topic'      => 'Our Blog Posts',
    'blog_info'       => 'It have shared wonderful blogs recently',
    'middle_banner_topic' => 'Being pretty and intelligent with both',
    'middle_banner_info'  => '',
    'middle_read_more'    => 'Read More',
    'middle_contact_us'   => 'Contact Us',
    'middle_customers_topic' => 'Customers Say',
    'middle_customers_info'  => 'What people say about us',
    'newsletter_topic'    => 'Subscribe to our Newsletter',
    'newsletter_info'     => 'let us know，you will get an economical class',
    'subscribe'           => 'subscription',
    'email_info'          => 'Enter your email address'
];
