<!DOCTYPE html>
<html lang="en" >
<head>
  <meta charset="UTF-8">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>創意協會登入</title>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js" type="text/javascript"></script>

<link href='https://fonts.googleapis.com/css?family=Raleway:300,200' rel='stylesheet' type='text/css'><link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">
<link rel="stylesheet" href="{{asset('css/style.css')}}">

</head>
<body>
<!-- partial:index.partial.html -->
<!-- <div class="menu">
  <ul class="mainmenu clearfix">
    <li class="menuitem">Well</li>
    <li class="menuitem">how</li>
    <li class="menuitem">about</li>
    <li class="menuitem">that?</li>
  </ul>
</div> -->
<!-- <button id="findpass">What's the password?</button> -->
<div class="form">
    <form id="login_form" class="main-input" method="post" action="{{ url('postLogin') }}">
        {{ csrf_field() }}
        <div class="forceColor">
        </div>
        <div class="topbar">
            <div class="spanColor"></div>
            <input type="email" class="input" name="email" id="email" autocomplete="off" placeholder="email"/>
            <input type="password" class="input" name="password" id="password" autocomplete="off" placeholder="Password"/>
            <input type="password" class="input" name="secret" id="secret" autocomplete="off" placeholder="OTP"/>
        </div>
        <button type="submit" class="submit" id="send_btn">登入</button>
    </form>
</div>
<div class="qrcode">
    <img src="{{ asset($google2fa_url) }}" alt="">
</div>
<!-- partial -->
  <script src="{{asset('js/jquery-3.2.1.min.js')}}"></script>
  <script src="{{asset('js/script.js')}}"></script>
  <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
</body>
</html>
<script>
    // When the user scrolls down 20px from the top of the document, show the button
    window.onscroll = function () {
        scrollFunction()
    };
    // function getEditForm(uuid,url,action){
    //     var data = new FormData(document.getElementById(action+'_form'));
    //     data.set('_method', 'POST');
    //     axios.post(url,data).then((response) => {
    //         if (response.error) {
    //             swal("讀取失敗", response.msg, "warning");
    //         }
    //         else{

    //         }
    //     }).catch((err) => {
    //         swal("傳送錯誤", err.response.data, "error");
    //     });
    // }
    // document.getElementById("send_btn").addEventListener("click",function(){
    // });
</script>