@if(Auth::check())
    <!-- {{ $user = Auth::user()->email }} -->
@endif
@extends('layouts.master')

@section('title', __('index.title'))

@section('content')
    <div class="main-content">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="overview-wrap">
                            <h2 class="title-1">營運狀況</h2>
                            <!-- <button class="au-btn au-btn-icon au-btn--blue">
                                <i class="zmdi zmdi-plus"></i>add item</button> -->
                        </div>
                    </div>
                </div>
                <div class="row m-t-25">
                    <div class="col-sm-6 col-lg-3">
                        <div class="overview-item overview-item--c1">
                            <div class="overview__inner">
                                <div class="overview-box clearfix">
                                    <div class="icon">
                                        <i class="zmdi zmdi-account-o"></i>
                                    </div>
                                    <div class="text">
                                        <h2>--</h2>
                                        <span>累計瀏覽人數</span>
                                    </div>
                                </div>
                                <div class="overview-chart">
                                    <canvas id="widgetChart1"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-3">
                        <div class="overview-item overview-item--c2">
                            <div class="overview__inner">
                                <div class="overview-box clearfix">
                                    <div class="icon">
                                        <i class="zmdi zmdi-shopping-cart"></i>
                                    </div>
                                    <div class="text">
                                        <h2>{{ $customers }}</h2>
                                        <span>學員數</span>
                                    </div>
                                </div>
                                <div class="overview-chart">
                                    <canvas id="widgetChart2"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-3">
                        <div class="overview-item overview-item--c3">
                            <div class="overview__inner">
                                <div class="overview-box clearfix">
                                    <div class="icon">
                                        <i class="zmdi zmdi-calendar-note"></i>
                                    </div>
                                    <div class="text">
                                        <h2>{{ $contact_month }}</h2>
                                        <span>當月報名未處理</span>
                                    </div>
                                </div>
                                <div class="overview-chart">
                                    <canvas id="widgetChart3"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-3">
                        <div class="overview-item overview-item--c4">
                            <div class="overview__inner">
                                <div class="overview-box clearfix">
                                    <div class="icon">
                                        <i class="zmdi zmdi-money"></i>
                                    </div>
                                    <div class="text">
                                        <h2>--</h2>
                                        <span>課程總收入</span>
                                    </div>
                                </div>
                                <div class="overview-chart">
                                    <canvas id="widgetChart4"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="au-card recent-report">
                            <div class="au-card-inner">
                                <h3 class="title-2">近期報告</h3>
                                <div class="chart-info">
                                    <div class="chart-info__left">
                                        <div class="chart-note">
                                            <span class="dot dot--blue"></span>
                                            <span>近期瀏覽次數</span>
                                        </div>
                                        <div class="chart-note mr-0">
                                            <span class="dot dot--green"></span>
                                            <span>近期報名</span>
                                        </div>
                                    </div>
                                    <div class="chart-info__right">
                                        <div class="chart-statis">
                                            <span class="index incre">
                                                <i class="zmdi zmdi-long-arrow-up"></i>25%</span>
                                            <span class="label">近期瀏覽次數</span>
                                        </div>
                                        <div class="chart-statis mr-0">
                                            <span class="index decre">
                                                <i class="zmdi zmdi-long-arrow-down"></i>10%</span>
                                            <span class="label">近期報名</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="recent-report__chart">
                                    <canvas id="recent-rep-chart"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="au-card chart-percent-card">
                            <div class="au-card-inner">
                                <h3 class="title-2 tm-b-5">訂單轉換率</h3>
                                <div class="row no-gutters">
                                    <div class="col-xl-6">
                                        <div class="chart-note-wrap">
                                            <div class="chart-note mr-0 d-block">
                                                <span class="dot dot--blue"></span>
                                                <span>當月瀏覽次數</span>
                                            </div>
                                            <div class="chart-note mr-0 d-block">
                                                <span class="dot dot--red"></span>
                                                <span>當月報名</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-6">
                                        <div class="percent-chart">
                                            <canvas id="percent-chart"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="au-card au-card--no-shadow au-card--no-pad m-b-40">
                            <div class="au-card-title" style="background-image:url('images/bg-title-01.jpg');">
                                <div class="bg-overlay bg-overlay--blue"></div>
                                <h3>
                                    <i class="fa fa-quote-left"></i>帳號管理</h3>
                                <button class="au-btn-plus" onclick="getInsertForm()">
                                    <i class="zmdi zmdi-plus"></i>
                                </button>
                            </div>
                            <div class="au-task js-list-load">
                                <div class="au-task__title">
                                    <p><i class="zmdi zmdi-account-calendar"></i>  {{ $today }}</p>
                                </div>
                                <div id="task-list_edit" class="au-task-list js-scrollbar3" style="display: none">
                                </div>
                                <div id="task-list_view" class="au-task-list js-scrollbar3">
                                    @foreach ( $users_arr as $users_key => $users )
                                    <div class="au-task__item au-task__item--danger">
                                        <div class="au-task__item-inner">
                                            <h5 class="task">
                                                <a onclick="getEditForm('{{ $users['id'] }}')" href="javascript:void(0);">{{ $users['email'] }}</a>
                                            </h5>
                                            <span class="time">{{ $users['updated_at'] }}</span>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                                <div class="au-task__footer">
                                    <button class="au-btn au-btn-load js-load-btn">load more</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="au-card au-card--no-shadow au-card--no-pad m-b-40">
                            <div class="au-card-title" style="background-image:url('images/bg-title-02.jpg');">
                                <div class="bg-overlay bg-overlay--blue"></div>
                                <h3>
                                    <i class="zmdi zmdi-comment-text"></i>即時留言</h3>
                                <button class="au-btn-plus" onclick="backMassegeView();">
                                    <i class="zmdi zmdi-arrow-left"></i>
                                </button>
                            </div>
                            <div class="au-inbox-wrap js-inbox-wrap">
                                <div class="au-message js-list-load">
                                    <div class="au-message__noti">
                                        <p>你有
                                            <span>{{ $messages_not }}</span>

                                            則新訊息
                                        </p>
                                    </div>
                                    <div id="message-list_edit" class="au-message-list" style="display: none">
                                    </div>
                                    <div id="message-list_view" class="au-message-list">
                                        @foreach ( $messages as $message_key => $message )
                                        <div class="au-message__item {{ $message['unread'] }}" onclick="getMassegeView('{{ $message['uuid'] }}')">
                                            <div class="au-message__item-inner">
                                                <div class="au-message__item-text">
                                                    <div class="avatar-wrap online">
                                                        <div class="avatar">
                                                            <img src="{{ asset($message['photo']) }}" alt="">
                                                        </div>
                                                    </div>
                                                    <div class="text">
                                                        <h5 class="name">{{ $message['user'] }}</h5>
                                                        <p>{{ $message['last_message'] }}</p>
                                                    </div>
                                                </div>
                                                <div class="au-message__item-time">
                                                    <span>{{ $message['updated_at'] }}</span>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                    <div class="au-message__footer">
                                        <button class="au-btn au-btn-load js-load-btn">load more</button>
                                    </div>
                                </div>
                                <div class="au-chat">
                                    <div class="au-chat__title">
                                        <div class="au-chat-info">
                                            <div class="avatar-wrap online">
                                                <div class="avatar avatar--small">
                                                    <img id="title_img" src="" alt="">
                                                </div>
                                            </div>
                                            <span class="nick">
                                                <a id="title_name" href="javascript:void(0);"></a>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="au-chat__content">

                                    </div>
                                    <div class="au-chat-textfield">
                                        <form class="au-form-icon">
                                            <input id="send_messages" autocomplete="off" class="au-input au-input--full au-input--h65" type="text" placeholder="Type a message">
                                            <button type="button" id="input-button" class="au-input-icon" onclick="">
                                                <i class="fa fa-location-arrow"></i>
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
<script id='recei_content' type='text/x-jquery-tmpl'>
    <div class="recei-mess-wrap">
        <span class="mess-time">${ create_at }</span>
        <div class="recei-mess__inner">
            <div class="avatar avatar--tiny">
                <img src="${ photo }" alt="John Smith">
            </div>
            <div class="recei-mess-list" id="${ mess }">
                <div class="recei-mess">${ content }</div>
            </div>
        </div>
    </div>
</script>
<script id='send_content' type='text/x-jquery-tmpl'>
    <div class="send-mess-wrap">
        <span class="mess-time">${ create_at }</span>
        <div class="send-mess__inner">
            <div class="send-mess-list" id="${ mess }">
                <div class="send-mess">${ content }</div>
            </div>
        </div>
    </div>
</script>
<script id='continue_content' type='text/x-jquery-tmpl'>
    <div class="${ type }">${ content }</div>
</script>
<script id='user_content' type='text/x-jquery-tmpl'>
    <div class="card">
        <div class="card-header">
            <strong>帳號</strong> 管理
        </div>
        <div class="card-body card-block">
            <form id="user_form">
                {{ csrf_field() }}
                <div class="row form-group">
                    <div class="col col-md-12">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="far fa-envelope"></i>
                            </div>
                            <input type="email" autocomplete="off" id="input2-group1" name="email" placeholder="Email" class="form-control" value="${ email }">
                            <div class="input-group-addon"></div>
                        </div>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-12">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-euro"></i>
                            </div>
                            <input type="password" autocomplete="off" id="input3-group1" name="password" placeholder="輸入密碼" class="form-control">
                            <div class="input-group-addon"></div>
                        </div>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-12">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-euro"></i>
                            </div>
                            <input type="password" autocomplete="off" id="input3-group1" name="password_1" placeholder="再次輸入密碼" class="form-control">
                            <div class="input-group-addon"></div>
                        </div>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="select" class=" form-control-label">講師選擇</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <select id="speaker_id" name="speaker_id" class="form-control">
                        </select>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label class=" form-control-label">執行更新</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <p class="form-control-static">{{ $user }}</p>
                    </div>
                </div>
            </form>
        </div>
        <div class="card-footer">
            <button type="submit" onclick="sendForm('${ id }')" class="btn btn-success btn-sm">
                <i class="fa fa-dot-circle-o"></i> 送出
            </button>
            <button type="button" onclick="exchangeForm('task-list','view');" class="btn btn-secondary btn-sm">
                <i class="fa fa-ban"></i> 取消
            </button>
            <button type="reset" class="btn btn-warning btn-sm">
                <i class="fa fa-ban"></i> 重置
            </button>
            <button style="${ button }" type="button" onclick="sendDelete('${ email }','${ id }')" class="btn btn-danger btn-sm">
                <i class="fa fa-trash-o"></i> 刪除
            </button>
        </div>
    </div>
</script>
<script id='select_content' type='text/x-jquery-tmpl'>
    <option value="${ value }" ${ selected }>${ option_name }</option>
</script>
<script type="text/javascript">
    var send_continue = '';
    function exchangeForm(item,show){
        var action = show == 'edit'?'_edit':'_view';
        var action_back = show == 'edit'?'_view':'_edit';
        $('#'+item+action).fadeIn();
        $('#'+item+action_back).fadeOut();
    }
    function getInsertForm(){
        $('#task-list_edit').text('').html();
        exchangeForm('task-list','edit');
        axios.get('/home/getUser').then((response) => {
            if (response.data.error) {
                swal("讀取失敗", response.data.msg, "warning");
            }
            else{
                var object = {};
                object['email']  = '';
                object['button'] = 'display:none';
                $( '#user_content' ).tmpl( object ).appendTo( '#task-list_edit' );
                delete object;
                var content = response.data['select'];
                for(var content_key in content){
                    var object = {};
                    object['value']       = content[content_key]['uuid'];
                    object['selected']    = '';
                    object['option_name'] = content[content_key]['option_name'];
                    $( '#select_content' ).tmpl( object ).appendTo( '#speaker_id' );
                    delete object;
                }
            }
        }).catch(error => {
            console.log(error);
        });
    }
    function getEditForm(accessCode){
        $('#task-list_edit').text('').html();
        exchangeForm('task-list','edit');
        axios.get('/home/getUser/'+accessCode).then((response) => {
            if (response.data.error) {
                swal("讀取失敗", response.data.msg, "warning");
            }
            else{
                var object = {};
                object['email']  = response.data.email;
                object['button'] = '';
                object['id']     = accessCode;
                $( '#user_content' ).tmpl( object ).appendTo( '#task-list_edit' );
                delete object;
                var content = response.data['select'];
                for(var content_key in content){
                    var object = {};
                    object['value']       = content[content_key]['uuid'];
                    object['selected']    = content[content_key]['selected'];
                    object['option_name'] = content[content_key]['option_name'];
                    $( '#select_content' ).tmpl( object ).appendTo( '#speaker_id' );
                    delete object;
                }
            }
        }).catch(error => {
            console.log(error);
        });
    }
    function sendForm(accesscode){
        $url_move = accesscode == ''?'/home/insertUser':'/home/updateUser/'+accesscode;
        var data = new FormData(document.getElementById('user_form'));
        data.set('_method', 'POST');
        axios.post($url_move,data).then((response) => {
            if (response.data.error) {
                swal("讀取失敗", response.data.msg, "warning");
            }
            else{
                var success_msg = accesscode == '' || typeof(accesscode)=='undefined'?'新增成功':'更新成功';
                swal({
                    icon: "success",
                    title: success_msg,
                    text: '',
                    type: 'success'
                }).then(
                    function () {
                        exchangeForm('task-list','view');
                        window.location.reload();
                    }
                )
            }
        }).catch(error => {
            console.log(error);
        });
    }
    function sendDelete(name,accesscode){
        swal({
            icon: "warning",
            title: "確定刪除"+name+'?',
            buttons:{
                cancel: {
                    text: "取消刪除",
                    visible: true
                },
                confirm: {
                    text: "確定刪除",
                    visible: true
                }
            }
        }).then((result) => {
            if (result) {
                axios.post('/home/delete/'+accesscode).then((response) => {
                    if (response.data.error) {
                        swal("刪除失敗", response.data.msg, "warning");
                    }
                    else{
                        swal({
                            icon: "success",
                            title: '刪除成功',
                            text: '',
                            type: 'success'
                        }).then(
                            function () {
                                exchangeForm('task-list','view');
                                window.location.reload();
                            }
                        )
                    }
                }).catch(error => {
                    console.log(error);
                });
            }
        });
    }
    function getMassegeView(uuid){
        $('.au-chat__content').text('').html();
        axios.get('/home/message/'+uuid).then((response) => {
            if (response.data.error) {
                swal("取得訊息失敗", response.data.msg, "warning");
            }
            else{
                $('#title_img').attr('src', response.data.photo);
                $('#title_img').attr('alt', response.data.name);
                $('#title_name').text(response.data.name).html();
                $('#input-button').attr('onclick', "sendMassege('"+uuid+"')");
                if (response.data.detail) {
                    var content = response.data.detail;
                    var check_conyinue = '';
                    var append = '';
                    for(var content_key in content){
                        var object = {};
                        if (content[content_key]['send']) {
                            if (check_conyinue != 'send') {
                                check_conyinue  = 'send';
                                object['content']   = content[content_key]['send'];
                                object['mess']      = 'send_'+content_key;
                                object['create_at'] = content[content_key]['created_at'];
                                append = 'send_'+content_key;
                                $( '#send_content' ).tmpl( object ).appendTo( '.au-chat__content' );
                            }
                            else{
                                var object = {};
                                object['content'] = content[content_key]['send'];
                                object['type']    = 'send-mess';
                                $( '#continue_content' ).tmpl( object ).appendTo( '#'+append );
                            }
                        }
                        else{
                            if (check_conyinue != 'recei') {
                                check_conyinue  = 'recei';
                                append = 'recei';
                                object['content'] = content[content_key]['recei'];
                                object['photo']   = response.data.photo;
                                object['mess'] = 'recei_'+content_key;
                                object['create_at'] = content[content_key]['created_at'];
                                append = 'send_'+content_key;
                                $( '#recei_content' ).tmpl( object ).appendTo( '.au-chat__content' );
                            }
                            else{
                                var object = {};
                                object['content'] = content[content_key]['recei'];
                                object['type']    = 'recei-mess';
                                $( '#continue_content' ).tmpl( object ).appendTo( '#'+append );
                            }
                        }
                        delete object;
                    }
                }
            }
        }).catch(error => {
            console.log(error);
        });
    }
    function sendMassege(uuid){
        var send_messages = $('#send_messages').val();
        axios.post('/home/message/'+uuid,{'content': send_messages}).then((response) => {
            if (response.data.error) {
                swal("新增訊息失敗，請洽管理人員。", response.data.msg, "warning");
            }
            else{
                var dt = new Date();
                $('#send_messages').val('');
                var object = {};
                object['content'] = send_messages;
                if (send_continue == '') {
                    object['create_at'] = dt;
                    object['mess'] = 'send_messages';
                }
                else{
                    object['type'] = 'send-mess';
                }
                var append     = send_continue == ''?'.au-chat__content':'#send_messages';
                var setContent = send_continue == ''?'#send_content':'#continue_content';
                $( setContent ).tmpl( object ).appendTo( append );
                delete object;
            }
        }).catch(error => {
            console.log(error);
        });
    }
    function backMassegeView(){
        $('.au-inbox-wrap').attr('class','au-inbox-wrap js-inbox-wrap');
    }
</script>
