@if(Auth::check())
    <!-- {{ $user = Auth::user()->email }} -->
@endif
@extends('layouts.master')

@section('title', __('index.title'))

@section('content')
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9">
                    <div class="table-data__tool">
                        <div class="table-data__tool-left">
                            <h3 class="title-3 m-b-30">
                                <i class="zmdi zmdi-account-calendar"></i>報名資訊
                            </h3>
                        </div>
                        <div class="table-data__tool-right">
                        </div>
                    </div>
                    <div id="contact_edit" str class="table-responsive table--no-card m-b-30 form_edit">
                    </div>
                    <div id="contact_view" class="table-responsive table--no-card m-b-30">
                        <table class="table table-borderless table-striped table-earning">
                            <thead>
                                <tr>
                                    <th>報名時間</th>
                                    <th>報名編號</th>
                                    <th>名稱</th>
                                    <th class="text-right">電子郵件</th>
                                    <th class="text-right">價錢</th>
                                    <th class="text-right">狀態</th>
                                    <th class="text-right">有興趣課程</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ( $contacts as $contacts_key => $contact )
                                    <tr>
                                        <td>{{ $contact['created_at'] }}</td>
                                        <td>{{ $contact['uuid'] }}</td>
                                        <td>{{ $contact['name'] }}</td>
                                        <td class="text-right">{{ $contact['email'] }}</td>
                                        <td class="text-right">{{ $contact['phone'] }}</td>
                                        <td class="text-right"><span class="role {{ $contact['status_color'] }}">{{ $contact['status'] }}</span></td>
                                        <td class="text-right">{{ $contact['class'] }}</td>
                                        <td>
                                            <div class="table-data-feature">
                                                <button onclick="updateContact('{{ $contact['name'] }}','{{ $contact['uuid'] }}')" type="button" class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                                    <i class="zmdi zmdi-check"></i>
                                                </button>
                                                <button type="button" onclick="sendDelete('contacts','{{ $contact['uuid'] }}','{{ $contact['name'] }}')" class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                                    <i class="zmdi zmdi-delete"></i>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="table-data__tool">
                        <div class="table-data__tool-left">
                            <h3 class="title-3 m-b-30">
                                <i class="zmdi zmdi-account-calendar"></i>標語資訊
                            </h3>
                        </div>
                        <div class="table-data__tool-right">
                            <button onclick="getInsertForm('slogans','slogan','edit');" class="au-btn au-btn-icon au-btn--green au-btn--small">+新增標語</button>
                        </div>
                    </div>
                    <div class="au-card au-card--bg-blue au-card-top-countries m-b-30">
                        <div class="au-card-inner">
                            <div id="slogan_edit" class="table-responsive form_edit">
                            </div>
                            <div id="slogan_view" class="">
                                <table class="table table-top-countries">
                                    <tbody>
                                        @foreach ( $slogans as $slogans_key => $slogan )
                                        <tr>
                                            <td>{{ $slogan['title'] }}</td>
                                            <td class="text-right">{{ $slogan['created_at'] }}</td>
                                            <td>
                                                <div class="table-data-feature">
                                                    <button onclick="getEditForm('{{ $slogan['uuid'] }}','{{ secure_url('/main_form/edit/slogans') }}','slogan');" type="button" class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                                        <i class="zmdi zmdi-edit"></i>
                                                    </button>
                                                    <button type="button" onclick="sendDelete('slogans','{{ $slogan['uuid'] }}','{{ $slogan['title'] }}')" class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                                    <i class="zmdi zmdi-delete"></i>
                                                </button>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <!-- USER DATA-->
                    <div class="user-data m-b-30">
                        <div class="table-data__tool">
                            <div class="table-data__tool-left">
                                <h3 class="title-3 m-b-30">
                                    <i class="zmdi zmdi-account-calendar"></i>文章資訊</h3>
                            </div>
                            <div class="table-data__tool-right">
                                <button type="button" onclick="getInsertForm('articles','article','edit');" class="au-btn au-btn-icon au-btn--green au-btn--small">
                                    <i class="zmdi zmdi-plus"></i>新增文章</button>
                                <button class="inner inner-visible au-btn au-btn-icon au-btn--green au-btn--small"></button>
                            </div>
                        </div>
                        <div id="article_edit" class="table-responsive form_edit">
                        </div>
                        <div id="article_view" class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <td></td>
                                        <td>文章標題</td>
                                        <td>狀態</td>
                                        <td>建立時間</td>
                                        <td>動作</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ( $articles as $article_key => $article )
                                    <tr>
                                        <td></td>
                                        <td>
                                            <div class="table-data__info">
                                                <h6>{{ $article['title'] }}</h6>
                                                <span>
                                                    <a href="#">{{ $article['title_e'] }}</a>
                                                </span>
                                            </div>
                                        </td>
                                        <td>
                                            <span class="role {{ $article['type_color'] }}">{{ $article['type'] }}</span>
                                        </td>
                                        <td>
                                            <span class="block-email">{{ $article['created_at'] }}</span>
                                        </td>
                                        <td>
                                            <div class="table-data-feature">
                                                <button onclick="getEditForm('{{ $article['uuid'] }}','{{ secure_url('/main_form/edit/articles') }}','article');" type="button" class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                                    <i class="zmdi zmdi-edit"></i>
                                                </button>
                                                <button type="button" onclick="sendDelete('articles','{{ $article['uuid'] }}','{{ $article['title'] }}')" class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                                    <i class="zmdi zmdi-delete"></i>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="user-data__footer">
                            <!-- <button class="au-btn au-btn-load">load more</button> -->
                        </div>
                    </div>
                    <!-- END USER DATA-->
                </div>
                <div class="col-lg-6">
                    <!-- TOP CAMPAIGN-->
                    <div class="top-campaign">
                        <div class="table-data__tool">
                            <div class="table-data__tool-left">
                                <h3 class="title-3 m-b-30">
                                    <i class="zmdi zmdi-account-calendar"></i>部落格資訊</h3>
                            </div>
                            <div class="table-data__tool-right">
                                <button type="button" onclick="getInsertForm('blogs','blog','edit');" class="au-btn au-btn-icon au-btn--green au-btn--small">
                                    <i class="zmdi zmdi-plus"></i>新增部落格</button>
                            </div>
                        </div>
                        <div id="blog_edit" class="table-responsive form_edit">
                        </div>
                        <div id="blog_view" class="">
                            <table class="table table-top-campaign">
                                <tbody>
                                    @foreach ( $blogs as $blog_key => $blog )
                                    <tr>
                                        <td>{{ $blog['title'] }}</td>
                                        <td>{{ $blog['created_at'] }}</td>
                                        <td>
                                            <div class="table-data-feature">
                                                <button onclick="getEditForm('{{ $blog['uuid'] }}','{{ secure_url('/main_form/edit/blogs') }}','blog');" type="button" class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                                    <i class="zmdi zmdi-edit"></i>
                                                </button>
                                                <button type="button" onclick="sendDelete('blogs','{{ $blog['uuid'] }}','{{ $blog['name'] }}')" class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                                    <i class="zmdi zmdi-delete"></i>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!--  END TOP CAMPAIGN-->
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <!-- DATA TABLE -->
                    <div class="table-data__tool">
                        <div class="table-data__tool-left">
                            <div class="rs-select2--light rs-select2--md">
                                <h3 class="title-5 m-b-35">講師資訊</h3>
                            </div>
                            <div class="rs-select2--light rs-select2--sm">
                            </div>
                        </div>
                        <div class="table-data__tool-right">
                            <button type="button" onclick="getInsertForm('speakers','speaker','edit');" class="au-btn au-btn-icon au-btn--green au-btn--small">
                                <i class="zmdi zmdi-plus"></i>新增講師</button>
                        </div>
                    </div>
                    <div id="speaker_edit" class="table-responsive table-responsive-data2 form_edit">
                    </div>
                    <div id="speaker_view" class="table-responsive table-responsive-data2">
                        <table class="table table-borderless table-data3">
                            <thead>
                                <tr>
                                    <th>名稱</th>
                                    <th>職稱</th>
                                    <th>介紹</th>
                                    <th>創建時間</th>
                                    <th>FB</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ( $speakers as $speaker_key => $speaker )
                                <tr class="tr-shadow">
                                    <td>{{ $speaker['name'] }}</td>
                                    <td>
                                        <span class="block-email">{{ $speaker['job_title'] }}</span>
                                    </td>
                                    <td>{{ $speaker['introduction'] }}</td>
                                    <td>{{ $speaker['created_at'] }}</td>
                                    <td class="desc"><a href="{{ $speaker['link_fb'] }}">連結</a></td>
                                    <td>
                                        <div class="table-data-feature">
                                            <button onclick="getEditForm('{{ $speaker['uuid'] }}','{{ secure_url('/main_form/edit/speakers') }}','speaker');" type="button" class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                                <i class="zmdi zmdi-edit"></i>
                                            </button>
                                            <button type="button" onclick="sendDelete('speakers','{{ $speaker['uuid'] }}','{{ $speaker['name'] }}')" class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                                <i class="zmdi zmdi-delete"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="spacer"></tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- END DATA TABLE -->
                </div>
            </div>
            <div class="row m-t-30">
                <div class="col-md-12">
                    <!-- DATA TABLE -->
                    <div class="table-data__tool">
                        <div class="table-data__tool-left">
                            <div class="rs-select2--light rs-select2--md">
                                <h3 class="title-5 m-b-35">學員資訊</h3>
                            </div>
                            <div class="rs-select2--light rs-select2--sm">
                            </div>
                        </div>
                        <div class="table-data__tool-right">
                            <button type="button" onclick="getInsertForm('customers','customer','edit');" class="au-btn au-btn-icon au-btn--green au-btn--small">
                                <i class="zmdi zmdi-plus"></i>新增學員</button>
                        </div>
                    </div>
                    <div id="customer_edit" class="table-responsive table-responsive-data2 form_edit">
                    </div>
                    <div id="customer_view" class="table-responsive table-responsive-data2">
                        <table class="table table-borderless table-data3">
                            <thead>
                                <tr>
                                    <th>名稱</th>
                                    <th>職稱</th>
                                    <th>介紹</th>
                                    <th>創建時間</th>
                                    <th>課程</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ( $customers as $customers_key => $customer )
                                <tr class="tr-shadow">
                                    <td>{{ $customer['name'] }}</td>
                                    <td>
                                        <span class="block-email">{{ $customer['job_title'] }}</span>
                                    </td>
                                    <td>{{ $customer['introduction'] }}</td>
                                    <td>{{ $customer['created_at'] }}</td>
                                    <td class="desc">{{ $customer['class_id'] }}</td>
                                    <td>
                                        <div class="table-data-feature">
                                            <button onclick="getEditForm('{{ $customer['uuid'] }}','{{ secure_url('/main_form/edit/customers') }}','customer');" type="button" class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                                <i class="zmdi zmdi-edit"></i>
                                            </button>
                                            <button type="button" onclick="sendDelete('customers','{{ $customer['uuid'] }}','{{ $customer['name'] }}')" class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                                <i class="zmdi zmdi-delete"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="spacer"></tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- END DATA TABLE -->
                </div>
            </div>
            <div class="row m-t-30">
                <div class="col-md-12">
                    <div class="table-data__tool">
                        <div class="table-data__tool-left">
                            <div class="rs-select2--light rs-select2--md">
                                <h3 class="title-5 m-b-35">課程資訊</h3>
                            </div>
                            <div class="rs-select2--light rs-select2--sm">
                            </div>
                        </div>
                        <div class="table-data__tool-right">
                            <button type="button" onclick="getInsertForm('classes','class','edit');" class="au-btn au-btn-icon au-btn--green au-btn--small">
                                <i class="zmdi zmdi-plus"></i>新增課程</button>
                        </div>
                    </div>
                    <!-- DATA TABLE-->
                    <div id="class_edit" class="table-responsive m-b-40 form_edit">
                    </div>
                    <div id="class_view" class="table-responsive m-b-40">
                        <table class="table table-borderless table-data3">
                            <thead>
                                <tr>
                                    <th>時間</th>
                                    <th>名稱</th>
                                    <th>描述</th>
                                    <th>狀態</th>
                                    <th>課程價錢</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ( $classes as $classes_key => $class )
                                    <tr>
                                        <td>{{ $class['created_at'] }}</td>
                                        <td>{{ $class['name'] }}</td>
                                        <td>{{ $class['introduction'] }}</td>
                                        <td class="process">{{ $class['classes_type'] }}</td>
                                        <td>{{ $class['price'] }}</td>
                                        <td>
                                            <div class="table-data-feature">
                                                <button onclick="getEditForm('{{ $class['uuid'] }}','{{ secure_url('/main_form/edit/classes') }}','class');" type="button" class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                                    <i class="zmdi zmdi-edit"></i>
                                                </button>
                                                <button type="button" onclick="sendDelete('classes','{{ $class['uuid'] }}','{{ $class['name'] }}')" class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                                    <i class="zmdi zmdi-delete"></i>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- END DATA TABLE-->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<script src="{{asset('js/jquery-3.2.1.min.js')}}"></script>
<script id='card' type='text/x-jquery-tmpl'>
    <div class="card">
        <div class="card-header">
            <strong>${ form_name }</strong>
        </div>
        <div class="card-body card-block">
            <form id="${ card_id }_form" action="${ form_action }" method="post" enctype="multipart/form-data" class="form-horizontal">
                {{ csrf_field() }}
            </form>
        </div>
    </div>
</script>
<script id='static_card' type='text/x-jquery-tmpl'>
    <div class="row form-group">
        <div class="col col-md-3">
            <label class=" form-control-label">${ key_name }</label>
        </div>
        <div class="col-12 col-md-9">
            <p class="form-control-static">${ value }</p>
        </div>
    </div>
</script>
<script id='input_card' type='text/x-jquery-tmpl'>
    <div class="row form-group">
        <div class="col col-md-3">
            <label for="text-input" class=" form-control-label">${ key_name }</label>
        </div>
        <div class="col-12 col-md-9">
            <input type="text" id="text-input" name="${ name }" autocomplete="off" class="form-control" value="${ value }">
        </div>
    </div>
</script>
<script id='select_card' type='text/x-jquery-tmpl'>
    <div class="row form-group">
        <div class="col col-md-3">
            <label for="select" class=" form-control-label">${ key_name }</label>
        </div>
        <div class="col-12 col-md-9">
            <select id="${ name }_select" name="${ name }" onchange="selectChange(this);" class="form-control">
            </select>
        </div>
    </div>
</script>
<script id='select_content_card' type='text/x-jquery-tmpl'>
    <option value="${ value }" ${ selected }>${ option_name }</option>
</script>
<script id='textarea_card' type='text/x-jquery-tmpl'>
    <div class="row form-group">
        <div class="col col-md-3">
            <label for="textarea-input" class=" form-control-label">${ key_name }</label>
        </div>
        <div class="col-12 col-md-9">
            <textarea name="${ name }" rows="9" class="form-control">${ value }</textarea>
        </div>
    </div>
</script>
<script id='radios_card' type='text/x-jquery-tmpl'>
    <div class="row form-group">
        <div class="col col-md-3">
            <label class=" form-control-label">${ key_name }</label>
        </div>
        <div class="col col-md-9">
            <div class="form-check-inline form-check">
                <label for="inline-radio1" class="form-check-label ">
                    <input type="radio" id="inline-radio1" name="${ name }" value="1" class="form-check-input" checked>啟用
                </label>
                <label for="inline-radio2" class="form-check-label ">
                    <input type="radio" id="inline-radio2" name="${ name }" value="2" class="form-check-input">停用
                </label>
            </div>
        </div>
    </div>
</script>
<script id='file_card' type='text/x-jquery-tmpl'>
    <div class="row form-group">
        <div class="col col-md-3">
            <label for="file-input" class=" form-control-label">${ key_name }</label>
        </div>
        <div class="col-12 col-md-9">
            <input type="file" id="file-input" name="${ name }" class="form-control-file">
            ${ value }
        </div>
    </div>
</script>
<script id='submit_card' type='text/x-jquery-tmpl'>
    <button type="button" onclick="sendForm('${ model }','${ action }','${ uuid }')" class="btn btn-primary btn-sm">
        <i class="fa fa-dot-circle-o"></i> 送出
    </button>
    <button type="button" onclick="exchangeForm('${ action }','view');" class="btn btn-secondary btn-sm">
        <i class="fa fa-ban"></i> 取消
    </button>
    <button type="reset" class="btn btn-danger btn-sm">
        <i class="fa fa-ban"></i> 重置
    </button>
</script>
<script type="text/javascript">
    function setEditHide(){
        $('.form_edit').hide();
    }
    function exchangeForm(item,show){
        var action = show == 'edit'?'_edit':'_view';
        var action_back = show == 'edit'?'_view':'_edit';
        $('#'+item+action).fadeIn();
        $('#'+item+action_back).fadeOut();
    }
    $( document ).ready(function() {
        setEditHide();
    });
    function getInsertForm(url,action,show){
        $('.form_edit').text('').html();
        if (show === 'edit') {
            axios.get('/main_form/'+url).then((response) => {
                if (response.data.error) {
                    swal("讀取失敗", response.data.msg, "warning");
                }
                else{
                    exchangeForm(action,show);
                    var object = {};
                    //Form Name
                    object['form_name']   = response['data']['form_name'];
                    object['card_id']     = action;
                    object['form_action'] = '/main_form/insert/'+url;
                    $( '#card' ).tmpl( object ).appendTo( '#'+action+'_edit' );
                    delete object;
                    var data = response.data.data_view;
                    for(var key in data){
                        var object = {};
                        object['key_name'] = response.data['lang'][key];
                        object['name']     = key;
                        object['value']    = data[key] === 'static'?'{{ $user }}':'';
                        $( '#'+data[key]+'_card' ).tmpl( object ).appendTo( '#'+action+'_form' );
                        delete object;
                        if (data[key] === 'select') {
                            var select = response.data.select[key];
                            for(var uuid_key in select){
                                if (key === 'job_title' || key === 'job_title_e') {
                                    if (uuid_key != 'name') {
                                        continue;
                                    }
                                    var content = select['name'];
                                    for(var content_key in content){
                                        var object = {};
                                        object['value']       = content_key;
                                        object['selected']    = '';
                                        object['option_name'] = content[content_key];
                                        if (object['value'] == '' || typeof(object['value'])=='undefined') {
                                            continue;
                                        }
                                        $( '#select_content_card' ).tmpl( object ).appendTo( '#'+key+'_select' );
                                    }
                                }
                                else{
                                    var object = {};
                                    object['value']       = response.data.select[key][uuid_key]['uuid'];
                                    object['selected']    = '';
                                    object['option_name'] = response.data.select[key][uuid_key]['name'];
                                    if (object['value'] == '' || typeof(object['value'])=='undefined') {
                                        continue;
                                    }
                                    $( '#select_content_card' ).tmpl( object ).appendTo( '#'+key+'_select' );
                                }
                                delete object;
                            }
                        }
                    }
                    var object = {};
                    object['model']   = url;
                    object['uuid']    = '';
                    object['action']  = action;
                    $( '#submit_card' ).tmpl( object ).appendTo( '#'+action+'_form' );
                    delete object;
                }
            }).catch(error => {
                console.log(error);
            });
        }
        else{
            exchangeForm(action,show);
        }
    }
    function getEditForm(uuid,url,action){
        $('.form_edit').text('').html();
        axios.get(url+'/'+uuid).then((response) => {
            if (response.data.error) {
                swal("讀取失敗", response.data.msg, "warning");
            }
            else{
                exchangeForm(action,'edit');
                var object = {};
                //Form Name
                object['form_name']   = response['data']['form_name'];
                object['card_id']     = action;
                object['form_action'] = '/main_form/insert/'+url;
                $( '#card' ).tmpl( object ).appendTo( '#'+action+'_edit' );
                delete object;
                var data = response.data.data_view;
                for(var key in data){
                    var object = {};
                    object['key_name'] = response.data['lang'][key];
                    object['name']     = key;
                    object['value']    = data[key] === 'static'?'{{ $user }}':response.data['value'][key];
                    $( '#'+data[key]+'_card' ).tmpl( object ).appendTo( '#'+action+'_form' );
                    if (data[key] === 'radios') {
                        $("input[name="+key+"][value='"+response.data['value'][key]+"']").attr('checked',true);
                    }
                    delete object;
                    if (data[key] === 'select') {
                        var select = response.data.select[key];
                        for(var uuid_key in select){
                            if (key === 'job_title' || key === 'job_title_e') {
                                if (uuid_key != 'name') {
                                    continue;
                                }
                                var content = select['name'];
                                for(var content_key in content){
                                    var object = {};
                                    var selected = content_key == select['value'][content_key]?'selected':'';
                                    object['value']       = content_key;
                                    object['selected']    = selected;
                                    object['option_name'] = content[content_key];
                                    if (object['value'] == '' || typeof(object['value'])=='undefined') {
                                        continue;
                                    }
                                    $( '#select_content_card' ).tmpl( object ).appendTo( '#'+key+'_select' );
                                }
                            }
                            else{
                                var object = {};
                                object['value']       = response.data.select[key][uuid_key]['uuid'];
                                object['selected']    = response.data.select[key][uuid_key]['selected'];
                                object['option_name'] = response.data.select[key][uuid_key]['name'];
                                if (object['value'] == '' || typeof(object['value'])=='undefined') {
                                    continue;
                                }
                                $( '#select_content_card' ).tmpl( object ).appendTo( '#'+key+'_select' );
                            }
                            delete object;
                        }
                    }
                }
                var object = {};
                object['model']   = url;
                object['uuid']    = uuid;
                object['action']  = action;
                $( '#submit_card' ).tmpl( object ).appendTo( '#'+action+'_form' );
                delete object;
            }
        }).catch(error => {
            console.log(error);
        });
    }
    function selectChange(obj){
        $('#job_title_select').val($(obj).val());
        $('#job_title_e_select').val($(obj).val());
    }
    function sendForm(url,action,uuid){
        var stmt_action = action == 'class' ? 'classe': action;
        $url_move = uuid == ''?'/main_form/insert/'+url:'/main_form/update/'+stmt_action+'s/'+uuid;
        var data = new FormData(document.getElementById(action+'_form'));
        data.set('_method', 'POST');
        axios.post($url_move,data).then((response) => {
            if (response.data.error) {
                swal("讀取失敗", response.data.msg, "warning");
            }
            else{
                var success_msg = uuid == '' || typeof(uuid)=='undefined'?'新增成功':'更新成功';
                swal({
                    icon: "success",
                    title: success_msg,
                    text: '',
                    type: 'success'
                }).then(
                    function () {
                        exchangeForm(action,'view');
                        window.location.reload();
                    }
                )
            }
        }).catch(error => {
            console.log(error);
        });
    }
    function sendDelete(action,uuid,name){
        swal({
            icon: "warning",
            title: "確定刪除"+name+'?',
            buttons:{
                cancel: {
                    text: "取消刪除",
                    visible: true
                },
                confirm: {
                    text: "確定刪除",
                    visible: true
                }
            }
        }).then((result) => {
            if (result) {
                axios.post('/main_form/delete/'+action,{'uuid': uuid}).then((response) => {
                    if (response.data.error) {
                        swal("刪除失敗", response.data.msg, "warning");
                    }
                    else{
                        swal({
                            icon: "success",
                            title: '刪除成功',
                            text: '',
                            type: 'success'
                        }).then(
                            function () {
                                exchangeForm(action,'view');
                                window.location.reload();
                            }
                        )
                    }
                }).catch(error => {
                    console.log(error);
                });
            }
        });
    }
    function updateContact(name,uuid){
        swal('更新之後就無法再對此筆記錄在執行動作',{
            icon: "warning",
            title: "確定要更新報名資訊: "+name+'?',
            buttons:{
                cancel: {
                    text: "取消",
                    visible: true
                },
                confirm: {
                    text: "確定",
                    visible: true
                }
            }
        }).then((result) => {
            if (result) {
                axios.post('/main_form/contact/update',{'uuid': uuid}).then((response) => {
                    if (response.data.error) {
                        swal("更新報名資訊失敗", response.data.msg, "warning");
                    }
                    else{
                        swal({
                            icon: "success",
                            title: '更新報名資訊成功',
                            text: '',
                            type: 'success'
                        }).then(
                            function () {
                                window.location.reload();
                            }
                        )
                    }
                }).catch(error => {
                    console.log(error);
                });
            }
        });
    }
</script>