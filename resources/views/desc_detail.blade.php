@if(Auth::check())
    <!-- {{ $user = Auth::user()->email }} -->
@endif
@extends('layouts.master')

@section('title', __('index.title'))

@section('content')
<div class="main-content">
	<div class="section__content section__content--p30">
		<div class="container-fluid">
			<div class="row">
				<!-- /# column -->
				<div class="col-lg-6">
					<div id="article_edit" class="card form_edit"></div>
					@foreach ( $articles as $article_key => $article )
					<div id="article_view" class="card">
						<div class="card-header">
							<h4><i class="zmdi zmdi-account-calendar"></i> {{ $article['title'] }}</h4>
						</div>
						<div class="card-body">
							<p class="text-muted m-b-15">
								<code>&lt;內容="{{ $article['content'] }}"&gt;</code></p>
							<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
								@foreach ( $article['msg'] as $article_msg_key => $article_msg )
								<li class="nav-item">
									<a class="nav-link {{ $article_msg['active'] ?? '' }}" href="#" onclick="editCardForm('{{ $article_msg['uuid'] }}','article','{{ $article['title'] }}')" role="tab" aria-controls="pills-home"
									 aria-selected="true">文章{{ $article_msg_key }}</a>
								</li>
								@endforeach
							</ul>
							<button onclick="addCardForm('{{ $article['uuid'] }}','article','{{ $article['title'] }}');" class="au-btn au-btn-icon au-btn--green au-btn--small">+</button>
						</div>
					</div>
					@endforeach
				</div>
				<div class="col-lg-6">
					<div id="blog_edit" class="card form_edit"></div>
					@foreach ( $blogs as $blog_key => $blog )
					<div id="blog_view" class="card">
						<div class="card-header">
							<h4><i class="zmdi zmdi-account-calendar"></i> {{ $blog['title'] }}</h4>
						</div>
						<div class="card-body">
							<p class="text-muted m-b-15">
								<code>{{ $blog['content'] }}</code></p>
							<!-- Centered Tabs -->
							<ul class="nav nav-pills nav-justified mb-3 mt-2" id="pills-tab" role="tablist">
								@foreach ( $blog['msg'] as $blog_msg_key => $blog_msg )
								<li class="nav-item">
									<a class="nav-link {{ $blog_msg['active'] ?? '' }}" id="pills-home-tab" href="#" onclick="editCardForm('{{ $blog_msg['uuid'] }}','blog','{{ $blog['title'] }}')" role="tab" aria-controls="pills-home"
									 aria-selected="true">部落格{{ $blog_msg_key }}</a>
								</li>
								@endforeach
							</ul>
							<button onclick="addCardForm('{{ $blog['uuid'] }}','blog','{{ $blog['title'] }}');" class="au-btn au-btn-icon au-btn--green au-btn--small">+</button>
						</div>
					</div>
					@endforeach
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
<script src="{{asset('js/jquery-3.2.1.min.js')}}"></script>
<script id='article_card' type='text/x-jquery-tmpl'>
	<div class="card">
        <div class="card-header">
            <strong>新增文章細節</strong>
        </div>
        <div class="card-body card-block">
            <form id="article_form" method="post" enctype="multipart/form-data" class="form-horizontal">
                {{ csrf_field() }}
                <div class="row form-group">
			        <div class="col col-md-3">
			            <label class=" form-control-label">文章名稱</label>
			        </div>
			        <div class="col-12 col-md-9">
			            <p class="form-control-static">${ name }</p>
			        </div>
			        <input type="hidden" name="article_id" value="${ uuid }">
			    </div>
			    <div class="row form-group">
			        <div class="col col-md-3">
			            <label for="textarea-input" class=" form-control-label">文章中文內容</label>
			        </div>
			        <div class="col-12 col-md-9">
			            <textarea name="content" rows="9" class="form-control">${ content_value }</textarea>
			        </div>
			    </div>
			    <div class="row form-group">
			        <div class="col col-md-3">
			            <label for="textarea-input" class=" form-control-label">文章英文內容</label>
			        </div>
			        <div class="col-12 col-md-9">
			            <textarea name="content_e" rows="9" class="form-control">${ content_e_value }</textarea>
			        </div>
			    </div>
			    <div class="row form-group">
			        <div class="col col-md-3">
			            <label for="file-input" class=" form-control-label">文章照片</label>
			        </div>
			        <div class="col-12 col-md-9">
			            <input type="file" id="file-input" name="photo" class="form-control-file">
			            ${ photo_value }
			        </div>
			    </div>
			    <div class="row form-group">
			        <div class="col col-md-3">
			            <label for="text-input" class=" form-control-label">照片編號</label>
			        </div>
			        <div class="col-12 col-md-9">
			            <input type="text" id="text-input" name="photo_desc" autocomplete="off" class="form-control" value="${ photo_desc_value }">
			        </div>
			    </div>
			    <div class="row form-group">
			        <div class="col col-md-3">
			            <label class=" form-control-label">文章排列類型</label>
			        </div>
			        <div class="col col-md-9">
			            <div class="form-check-inline form-check">
			                <label for="inline-radio1" class="form-check-label ">
			                    <input type="radio" id="inline-radio1" name="type" value="0" class="form-check-input" checked>照片在前
			                </label>
			                <label for="inline-radio2" class="form-check-label ">
			                    <input type="radio" id="inline-radio2" name="type" value="1" class="form-check-input">照片在後
			                </label>
			            </div>
			        </div>
			    </div>
			    <div class="row form-group">
			        <div class="col col-md-3">
			            <label class=" form-control-label">文章創建/更新</label>
			        </div>
			        <div class="col-12 col-md-9">
			            <p class="form-control-static">{{ $user }}</p>
			        </div>
			    </div>
			    <button type="button" onclick="sendCardForm('${ action }','${ uuid }','${ child_uuid }')" class="btn btn-success btn-sm">
			        <i class="fa fa-dot-circle-o"></i> 送出
			    </button>
			    <button type="button" onclick="exchangeForm('${ action }','view');" class="btn btn-secondary btn-sm">
			        <i class="fa fa-ban"></i> 取消
			    </button>
			    <button type="reset" class="btn btn-warning btn-sm">
			        <i class="fa fa-ban"></i> 重置
			    </button>
			    <button type="button" onclick="delData('${ action }','${ child_uuid }','${ name }的細節')" class="btn btn-warning btn-sm" style="display:none">
			        <i class="fa fa-trash-o"></i> 刪除
			    </button>
            </form>
        </div>
    </div>
</script>
<script id='blog_card' type='text/x-jquery-tmpl'>
	<div class="card">
        <div class="card-header">
            <strong>新增部落格細節</strong>
        </div>
        <div class="card-body card-block">
            <form id="blog_form" method="post" enctype="multipart/form-data" class="form-horizontal">
                {{ csrf_field() }}
                <div class="row form-group">
			        <div class="col col-md-3">
			            <label class=" form-control-label">部落格名稱</label>
			        </div>
			        <div class="col-12 col-md-9">
			            <p class="form-control-static">${ name }</p>
			        </div>
			        <input type="hidden" name="blog_id" value="${ uuid }">
			    </div>
			    <div class="row form-group">
			        <div class="col col-md-3">
			            <label for="textarea-input" class=" form-control-label">部落格中文內容</label>
			        </div>
			        <div class="col-12 col-md-9">
			            <textarea name="content" rows="9" class="form-control">${ content_value }</textarea>
			        </div>
			    </div>
			    <div class="row form-group">
			        <div class="col col-md-3">
			            <label for="textarea-input" class=" form-control-label">部落格英文內容</label>
			        </div>
			        <div class="col-12 col-md-9">
			            <textarea name="content_e" rows="9" class="form-control">${ content_e_value }</textarea>
			        </div>
			    </div>
			    <div class="row form-group">
			        <div class="col col-md-3">
			            <label class=" form-control-label">部落格創建/更新</label>
			        </div>
			        <div class="col-12 col-md-9">
			            <p class="form-control-static">{{ $user }}</p>
			        </div>
			    </div>
			    <button type="button" onclick="sendCardForm('${ action }','${ uuid }','${ child_uuid }')" class="btn btn-success btn-sm">
			        <i class="fa fa-dot-circle-o"></i> 送出
			    </button>
			    <button type="button" onclick="exchangeForm('${ action }','view');" class="btn btn-secondary btn-sm">
			        <i class="fa fa-ban"></i> 取消
			    </button>
			    <button type="reset" class="btn btn-warning btn-sm">
			        <i class="fa fa-ban"></i> 重置
			    </button>
			    <button type="button" onclick="delData('${ action }','${ child_uuid }','${ name }的細節')" class="btn btn-warning btn-sm" style="display:none">
			        <i class="fa fa-trash-o"></i> 刪除
			    </button>
            </form>
        </div>
    </div>
</script>
<script type="text/javascript">
	function setEditHide(){
        $('.form_edit').hide();
    }
    function exchangeForm(item,show){
    	$('.form_edit').text('').html();
        var action = show == 'edit'?'_edit':'_view';
        var action_back = show == 'edit'?'_view':'_edit';
        $('#'+item+action).fadeIn();
        $('#'+item+action_back).fadeOut();
    }
    $( document ).ready(function() {
        setEditHide();
    });
    function addCardForm(uuid,action,name){
    	exchangeForm(action,'edit');
    	var object = {};
    	object['name'] = name;
		object['uuid'] = uuid;
		object['content_value']   = '';
		object['content_e_value'] = '';
		object['action'] = action;
		object['child_uuid'] = '';
        if (action === 'article') {
        	object['photo_value'] = '';
			object['photo_desc_value'] = '';
        }
        $( '#'+action+'_card' ).tmpl( object ).appendTo( '#'+action+'_edit' );
        delete object;
    }
    function delData(action,uuid,name){
    	swal({
            icon: "warning",
            title: "確定刪除"+name+'?',
            buttons:{
                cancel: {
                    text: "取消刪除",
                    visible: true
                },
                confirm: {
                    text: "確定刪除",
                    visible: true
                }
            }
        }).then((result) => {
            if (result) {
                axios.post('/main_form/delete/'+action,{'uuid': uuid}).then((response) => {
                    if (response.data.error) {
                        swal("刪除失敗", response.data.msg, "warning");
                    }
                    else{
                        swal({
                            icon: "success",
                            title: '刪除成功',
                            text: '',
                            type: 'success'
                        }).then(
                            function () {
                                exchangeForm(action,'view');
                                window.location.reload();
                            }
                        )
                    }
                }).catch(error => {
                    console.log(error);
                });
            }
        });
    }
    function editCardForm(uuid,action,name){
    	axios.get('/detail/edit/'+action+'/'+uuid).then((response) => {
            if (response.data.error) {
                swal("讀取失敗", response.data.msg, "warning");
            }
            else{
                exchangeForm(action,'edit');
                var data = response.data.value;
                for(var key in data){
                    var object = {};
			    	object['name'] = name;
					object['uuid'] = uuid;
					object['child_uuid'] = data['uuid'];
					object['content_value']   = data['content'];
					object['content_e_value'] = data['content_e'];
					object['action'] = action;
			        if (action === 'article') {
			        	object['photo_value'] = data['photo'];
						object['photo_desc_value'] = data['photo_desc'];
						if (key === 'type') {
							$("input[name="+key+"][value='"+data['type']+"']").attr('checked',true);
						}
			        }
                }
                $( '#'+action+'_card' ).tmpl( object ).appendTo( '#'+action+'_edit' );
			    delete object;
            }
        }).catch(error => {
            console.log(error);
        });
    }
    function sendCardForm(action,mian_uuid,uuid){
    	$url_move = uuid == ''?'/detail/insert/'+action:'/detail/update/'+action+'/'+uuid;
        var data = new FormData(document.getElementById(action+'_form'));
        data.set('_method', 'POST');
        axios.post($url_move,data).then((response) => {
            if (response.data.error) {
                swal("讀取失敗", response.data.msg, "warning");
            }
            else{
                var success_msg = uuid == '' || typeof(uuid)=='undefined'?'新增成功':'更新成功';
                swal({
                    icon: "success",
                    title: success_msg,
                    text: '',
                    type: 'success'
                }).then(
                    function () {
                        exchangeForm(action,'view');
                        window.location.reload();
                    }
                )
            }
        }).catch(error => {
            console.log(error);
        });
    }
</script>