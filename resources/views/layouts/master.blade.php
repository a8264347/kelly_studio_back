<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Title Page-->
    <title>@yield('title')</title>

    <!-- Fontfaces CSS-->
    <link href="{{asset('css/font-face.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('out/font-awesome-4.7/css/font-awesome.min.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('out/font-awesome-5/css/fontawesome-all.min.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('out/mdi-font/css/material-design-iconic-font.min.css')}}" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="{{asset('out/bootstrap-4.1/bootstrap.min.css')}}" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="{{asset('out/animsition/animsition.min.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('out//bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('out/wow/animate.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('out/css-hamburgers/hamburgers.min.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('out/slick/slick.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('out/select2/select2.min.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('out/perfect-scrollbar/perfect-scrollbar.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('out/vector-map/jqvmap.min.css')}}" rel="stylesheet" media="all">

    <link href="{{asset('out/fullcalendar-3.10.0/fullcalendar.css')}}" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="{{asset('css/theme.css')}}" rel="stylesheet" media="all">

</head>

<body class="animsition">

    <div class="page-wrapper">
        @include('layouts.header_mobile')
        @include('layouts.sidebar')
        <div class="page-container">
            @include('layouts.header_desktop')
            @yield('content')

            @include('layouts.copyright')
        </div>
    </div>

    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <!-- Jquery JS-->
    <script src="{{asset('out/jquery-3.2.1.min.js')}}"></script>
    <!-- Bootstrap JS-->
    <script src="{{asset('out/bootstrap-4.1/popper.min.js')}}"></script>
    <script src="{{asset('out/bootstrap-4.1/bootstrap.min.js')}}"></script>
    <!-- Vendor JS       -->
    <script src="{{asset('out/slick/slick.min.js')}}">
    </script>
    <script src="{{asset('out/wow/wow.min.js')}}"></script>
    <script src="{{asset('out/animsition/animsition.min.js')}}"></script>
    <script src="{{asset('out/bootstrap-progressbar/bootstrap-progressbar.min.js')}}">
    </script>
    <script src="{{asset('out/counter-up/jquery.waypoints.min.js')}}"></script>
    <script src="{{asset('out/counter-up/jquery.counterup.min.js')}}">
    </script>
    <script src="{{asset('out/circle-progress/circle-progress.min.js')}}"></script>
    <script src="{{asset('out/perfect-scrollbar/perfect-scrollbar.js')}}"></script>
    <script src="{{asset('out/chartjs/Chart.bundle.min.js')}}"></script>
    <script src="{{asset('out/select2/select2.min.js')}}"></script>
    <script src="http://ajax.microsoft.com/ajax/jquery.templates/beta1/jquery.tmpl.min.js"></script>

    <script src="{{asset('out/vector-map/jquery.vmap.js')}}"></script>
    <script src="{{asset('out/vector-map/jquery.vmap.min.js')}}"></script>
    <script src="{{asset('out/vector-map/jquery.vmap.sampledata.js')}}"></script>
    <script src="{{asset('out/vector-map/jquery.vmap.world.js')}}"></script>
    <script src="{{asset('out/vector-map/jquery.vmap.brazil.js')}}"></script>
    <script src="{{asset('out/vector-map/jquery.vmap.europe.js')}}"></script>
    <script src="{{asset('out/vector-map/jquery.vmap.france.js')}}"></script>
    <script src="{{asset('out/vector-map/jquery.vmap.germany.js')}}"></script>
    <script src="{{asset('out/vector-map/jquery.vmap.russia.js')}}"></script>
    <script src="{{asset('out/vector-map/jquery.vmap.usa.js')}}"></script>

    <script src="{{asset('out/fullcalendar-3.10.0/lib/moment.min.js')}}"></script>
    <script src="{{asset('out/fullcalendar-3.10.0/fullcalendar.js')}}"></script>

    <!-- Main JS-->
    <script src="{{asset('js/main.js')}}"></script>
</body>

</html>
<!-- end document-->
