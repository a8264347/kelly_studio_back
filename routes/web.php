<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => ['auth', 'restrictIp']], function () {
	Route::get('/home', 'IndexController@index')->name('home');
	Route::get('/home/getUser', 'IndexController@getUser')->name('home_get_user');
	Route::get('/home/getUser/{accessCode}', 'IndexController@getUserEdit')->name('home_get_user_edit');
	Route::post('/home/insertUser', 'IndexController@insertUser')->name('home_insert_user');
	Route::post('/home/updateUser/{accessCode}', 'IndexController@updateUser')->name('home_update_user');
	Route::post('/home/delete/{accessCode}', 'IndexController@delUser')->name('home_del_user');
	Route::get('/home/message/{uuid}', 'IndexController@getMassegeView')->name('home_message');
	Route::post('/home/message/{uuid}', 'IndexController@sendMassege')->name('home_send_message');
	Route::get('/main_form', 'FormController@index')->name('main_form');
	Route::get('/main_form/{model}', 'FormController@getInsertForm')->name('main_form_model');
	Route::get('/main_form/edit/{model}/{uuid}', 'FormController@getEditForm')->name('main_form_edit');
	Route::post('/main_form/insert/{model}', 'FormController@InsertForm')->name('main_form_insert');
	Route::post('/main_form/delete/{model}', 'FormController@delData')->name('main_form_del');
	Route::post('/main_form/update/{model}/{uuid}', 'FormController@updateData')->name('main_form_update');
	Route::post('/main_form/contact/update', 'FormController@checkContact')->name('contact_update');
	Route::get('/detail', 'DetailController@index')->name('detail');
	Route::get('/detail/edit/{model}/{uuid}', 'DetailController@getEditForm')->name('detail_edit');
	Route::post('/detail/insert/{model}', 'DetailController@InsertForm')->name('detail_insert');
	Route::post('/detail/delete/{model}', 'DetailController@delData')->name('detail_del');
	Route::post('/detail/update/{model}/{uuid}', 'DetailController@updateData')->name('detail_update');
	Route::get('/schedule', 'ScheduleController@index')->name('schedule');
	Route::get('/map', 'MapController@index')->name('map');
});
Route::group(['middleware' => 'restrictIp'], function () {
	Route::get('/', 'AuthController@getLogin')->name('login');
	Route::get('/logout', 'AuthController@logout')->name('logout');
	// Route::post('send2FASecret', 'AuthController@get2FASecret')->name('send2FASecret');
	Route::post('/postLogin', 'AuthController@postLogin')->name('postLogin');
});
