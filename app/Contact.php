<?php

namespace App;

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Contact extends Model
{
	protected $fillable = [
        "name",
        "email",
        "phone",
        "message",
        "status",
        "class_id"
    ];
    protected $table = 'contact';
    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->uuid = Str::uuid();
        });
    }
    public function getSchemaTable()
    {
        return $this->table;
    }
    protected function getTableColumns()
    {
        return Schema::getColumnListing($this->getSchemaTable());
    }
}
