<?php

namespace App;

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Customer extends Model
{
	protected $table = 'customers';
    protected $fillable = [
        'name',
        'name_e',
        'job_title',
        'job_title_e',
        'photo',
        'introduction',
        'introduction_e',
        'message',
        'class_id',
        'created_by',
        'updated_by'
    ];
    protected $access = array(
        'name',
        'name_e',
        'job_title',
        'job_title_e',
        'photo',
        'introduction',
        'introduction_e',
        'message',
        'class_id'
    );
    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->uuid = Str::uuid();
        });
    }
    protected function getAccessField(){
        return $this->access;
    }
    public function getSchemaTable()
    {
        return $this->table;
    }
    protected function getTableColumns()
    {
        return Schema::getColumnListing($this->getSchemaTable());
    }
}
