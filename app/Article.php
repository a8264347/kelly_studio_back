<?php

namespace App;

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Article extends Model
{
	protected $table = 'articles';
    protected $fillable = [
        'title',
        'title_e',
        'content',
        'content_e',
        'photo',
        'type',
        'created_by',
        'updated_by'
    ];
    protected $access = array(
        'title',
        'title_e',
        'content',
        'content_e',
        'photo',
        'type'
    );
    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->uuid = Str::uuid();
        });
    }
    protected function getAccessField(){
        return $this->access;
    }
    public function getSchemaTable()
    {
        return $this->table;
    }
    protected function getTableColumns()
    {
        return Schema::getColumnListing($this->getSchemaTable());
    }
}
