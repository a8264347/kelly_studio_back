<?php

namespace App\Http\Middleware;

use Closure;

class IpMiddleware
{
    public $restrictIps = ['157.230.255.117','127.0.0.1'];
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (!in_array($request->ip(), $this->restrictIps)){
            if (!in_array($_SERVER['HTTP_X_FORWARDED_FOR'], $this->restrictIps)) {
                return abort(404);
            }
            // return response()->json(["you don't have permission to access this application"]);
        }

        return $next($request);
    }
}
