<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;

class SloganRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|max:50',
            'title_e' => 'nullable|string|max:50',
            'short_introduction' => 'nullable|string|max:100',
            'short_introduction_e' => 'nullable|string|max:100',
            'content' => 'nullable|string',
            'content_e' => 'nullable|string',
            'mark_class' => 'nullable|string|max:36',
            'created_by' => 'nullable|integer|min:0|max:20',
            'updated_by' => 'nullable|integer|min:0|max:20'
        ];
    }
}
