<?php

namespace App\Http\Requests;

use App\Contact;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

class ContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // return [];
        return [
            'name'  => 'required|string|max:36',
            'email' => 'required|email|max:100',
            'phone' => 'nullable|string|max:100',
            'message' => 'nullable|string|max:300'
        ];
    }
}
