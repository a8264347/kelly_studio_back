<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\ArticleDesc;
use App\Blog;
use App\BlogMsg;
use Validator;
use App\Http\Requests\BlogMsgRequest;
use App\Http\Requests\ArticleDescRequest;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;
use Storage;

class DetailController extends Controller
{
    public function __construct(){
       parent::__construct();
    }
	public static $access_form = array(
		'blog_id','content_e','content','photo','photo_desc','type'
	);
    public function index(){
    	$data = array();
    	$articles  = Article::orderBy('id','desc')->get()->toArray();
		$blogs     = Blog::orderBy('id','desc')->get()->toArray();
        $articles_tmp = ArticleDesc::orderBy('article_id','asc')->get()->toArray();
        $blogs_tmp    = BlogMsg::orderBy('blog_id','asc')->get()->toArray();
        $articles_msg = array();
        $blogs_msg    = array();
        if (!empty($articles_tmp)) {
            $tmp_key = 0;
            $tmp_id  = 0;
            foreach ($articles_tmp as $key => $value) {
                if ($tmp_id != $value['article_id']) {
                    $tmp_key = 0;
                }
                $articles_msg[$value['article_id']][$tmp_key] = $value;
                $tmp_key++;
            }
        }
        if (!empty($blogs_tmp)) {
            foreach ($blogs_tmp as $key => $value) {
                if ($tmp_id != $value['blog_id']) {
                    $tmp_key = 0;
                }
                $blogs_msg[$value['blog_id']][$tmp_key] = $value;
                $tmp_key++;
            }
        }
		if (!empty($articles)) {
    		foreach ($articles as $key => $value) {
    			$articles[$key]['msg'] = isset($articles_msg[$value['id']])?$articles_msg[$value['id']]:array();
                if (!empty($articles_msg[$value['id']])) {
                    $articles[$key]['msg'][0]['active'] = $key == 'id' ? 'active' : '';
                }
    		}
    	}
    	if (!empty($blogs)) {
    		foreach ($blogs as $key => $value) {
    			$blogs[$key]['msg'] = isset($blogs_msg[$value['id']])?$blogs_msg[$value['id']]:array();
                if (!empty($blogs_msg[$value['id']])) {
                    $blogs[$key]['msg'][0]['active'] = $key == 'id' ? 'active' : '';
                }
    		}
    	}
        $data['articles'] = $articles;
        $data['blogs'] = $blogs;
    	return view('desc_detail',$data);
    }
    public function getEditForm(string $model,string $uuid){
    	$get_allow_key = array_keys(DetailController::$access_form);
    	switch ($model) {
    		case 'article':
    			$columns = ArticleDesc::where('uuid', '=', $uuid)->firstOrFail()->toArray();
    			break;
    		case 'blog':
    			$columns = BlogMsg::where('uuid', '=', $uuid)->firstOrFail()->toArray();
    			break;
    		default:
    			$return['error'] = 1;
    			$return['msg'] = '讀取資料錯誤!';
    			break;
    	}
    	if (!empty($columns)) {
    		foreach ($columns as $key => $value) {
    			if (in_array($key, $get_allow_key)) {
	    			if ($key == 'photo' && !empty($value)) {
	    				$stmt_value = explode('-', $value);
	    				$value = end($stmt_value);
	    			}
	    			$return['value'][$key] = $value;
	    		}
	    	}
    	}
    	return $return;
    }
    public function InsertForm(Request $request,string $model){
    	$return = array(
    		'error' => 0,
    		'msg'   => ''
    	);
    	$input = $request->all();
    	if ($request->hasFile('photo')) {
    		$url_return = $this->savePhoto($request->file('photo'));
        }
    	switch ($model) {
    		case 'article':
    			$article_id  = $request->input('article_id', 0);
    			$columns     = Article::where('uuid', '=', $article_id)->get()->toArray();
    			if (!empty($columns)) {
    				foreach ($columns as $columns_key => $columns_value) {
                        $request->merge( array( 'article_id' => $columns_value['id'] ) );
    					$input['article_id'] = $columns_value['id'];
    				}
    			}
    			$article   = new ArticleDescRequest();
    			$rules     = $article->rules();
			    $validator = Validator::make($input, $rules);
			    if ($validator->passes()) {
    				$access  = ArticleDesc::getAccessField();
			    	$data    = $request->only($access);
			    	$data['photo'] = isset($url_return)?$url_return:'';
			    	$data['created_by'] = auth()->user()->id;
    				$data['updated_by'] = auth()->user()->id;
			    	$article = ArticleDesc::create($data);
			    	$return['msg'] = $article;
			    	break;
				}
				$return['error'] = 1;
				$return['msg']   = '新增文章細節錯誤!'.$validator->errors();
    			break;
    		case 'blog':
    			$blog_id  = $request->input('blog_id', 0);
    			$columns  = Blog::where('uuid', '=', $blog_id)->get()->toArray();
    			if (!empty($columns)) {
    				foreach ($columns as $columns_key => $columns_value) {
                        $request->merge( array( 'blog_id' => $columns_value['id'] ) );
    					$input['blog_id'] = $columns_value['id'];
    				}
    			}
    			$blog      = new BlogMsgRequest();
    			$rules     = $blog->rules();
			    $validator = Validator::make($input, $rules);
			    if ($validator->passes()) {
    				$access  = BlogMsg::getAccessField();
			    	$data    = $request->only($access);
			    	$data['created_by'] = auth()->user()->id;
    				$data['updated_by'] = auth()->user()->id;
			    	$blog = BlogMsg::create($data);
			    	$return['msg'] = $blog;
			    	break;
				}
				$return['error'] = 1;
				$return['msg']   = '新增文章細節錯誤!'.$validator->errors();
    			break;
    		default:
    			$return['error'] = 1;
    			$return['msg'] = '新增細節錯誤!';
    			break;
    	}
		return json_encode($return);
    }
    public function delData(Request $request,string $model){
    	$return = array(
    		'error' => 0,
    		'msg'   => ''
    	);
    	$uuid = $request->input('uuid', 0);
    	if (!empty($uuid)) {
    		switch ($model) {
	    		case 'article':
	    			$columns = ArticleDesc::where('uuid', '=', $uuid)->delete();
	    			break;
	    		case 'blog':
	    			$columns = BlogMsg::where('uuid', '=', $uuid)->delete();
	    			break;
	    		default:
	    			$return['error'] = 1;
	    			$return['msg'] = '刪除資料錯誤!';
	    			break;
	    	}
    	}
    	return json_encode($return);
    }
    public function updateData(Request $request,string $model,string $uuid){
    	$input = $request->all();
    	if ($request->hasFile('photo')) {
    		$url_return = $this->savePhoto($request->file('photo'));
        }
    	switch ($model) {
    		case 'article':
    			$articles_update = ArticleDesc::where('uuid', '=', $uuid)->firstOrFail();
                $article_id = $articles_update->toArray();
                if (!empty($article_id)) {
                    $request->merge( array( 'article_id' => $article_id['id'] ) );
                    $input['article_id'] = $article_id['id'];
                }
    			$article   = new ArticleDescRequest();
    			$rules     = $article->rules();
			    $validator = Validator::make($input, $rules);
			    if ($validator->passes()) {
    				$access  = ArticleDesc::getAccessField();
			    	$data    = $request->only($access);
			    	if (isset($url_return)) {
			    		$data['photo'] = $url_return;
			    	}
    				$data['updated_by'] = auth()->user()->id;
			    	$article = $articles_update->update($data);
			    	$return['msg'] = $article;
			    	break;
				}
				$return['error'] = 1;
				$return['msg']   = '新增文章資料錯誤!'.$validator->errors();
    			break;
    		case 'blog':
    			$blog_update = BlogMsg::where('uuid', '=', $uuid)->firstOrFail();
                $blog_id     = $blog_update->toArray();
                if (!empty($blog_id)) {
                    $request->merge( array( 'blog_id' => $blog_id['id'] ) );
                    $input['blog_id'] = $blog_id['id'];
                }
    			$blog        = new BlogMsgRequest();
    			$rules       = $blog->rules();
			    $validator   = Validator::make($input, $rules);
			    if ($validator->passes()) {
    				$access  = BlogMsg::getAccessField();
			    	$data    = $request->only($access);
			    	if (isset($url_return)) {
			    		$data['photo'] = $url_return;
			    	}
    				$data['updated_by'] = auth()->user()->id;
    				$blog = $blog_update->update($data);
			    	$return['msg'] = $blog;
			    	break;
				}
				$return['error'] = 1;
				$return['msg']   = '新增文章資料錯誤!'.$validator->errors();
    			break;
    		default:
    			$return['error'] = 1;
    			$return['msg'] = '讀取資料錯誤!';
    			break;
    	}
		return json_encode($return);
    }
    public function savePhoto(UploadedFile $image){
    	$file_name = auth()->user()->id . '-'  . Str::random(8). '-' . $image->getClientOriginalName();
    	Storage::put($file_name, $image->get());
    	return 'storage/'.$file_name;
    }
}
