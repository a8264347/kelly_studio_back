<?php

namespace App\Http\Controllers;

use Session;
use PragmaRX\Google2FA\Google2FA;
use App\Http\Requests\ChangePasswordRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redis;
use App\Mail\Warning;
use Validator;
use Redirect;
use BaconQrCode\Renderer\ImageRenderer;
use BaconQrCode\Renderer\Image\ImagickImageBackEnd;
use BaconQrCode\Renderer\RendererStyle\RendererStyle;
use BaconQrCode\Writer;
use Illuminate\Filesystem\Filesystem;
use QrCode;
class AuthController extends Controller
{
    public function getLogin()
    {
        $file = new Filesystem;
        $file->cleanDirectory('qrcode');
        $google2fa = new Google2FA();
        $secretKey = $google2fa->generateSecretKey(32);
        Redis::set('2FA', $secretKey);
        $qrCodeUrl = $google2fa->getQRCodeUrl(
            'edmond.tong',
            'a8264347@gmail.com',
            $secretKey
        );
        // 图片路径
        $filePath = "qrcode/". sha1($secretKey) .'.png';
        // 生成图片
        $renderer = new ImageRenderer(
                    new RendererStyle(200),
                    new ImagickImageBackEnd()
        );
        $writer = new Writer($renderer);

        $writer->writeFile($qrCodeUrl, $filePath);
        $this->get2FASecret();
        return view('login',array('google2fa_url' => $filePath));
    }
    public function get2FASecret()
    {
        $google2fa = new Google2FA();
        $return = array(
            'error' => false,
            'msg'   => ''
        );
        $code   = $google2fa->getCurrentOtp(Redis::get('2FA'));
        $result = $this->send($code);
        // $result = $this->send(Redis::get('2FA'));
        if (!$result) {
            $return['error'] = true;
            $return['msg']   = '信件寄出失敗。';
        }
        return $return;
    }
    public function postLogin(Request $request)
    {
        $google2fa = new Google2FA();
    	$input = array(
            'email'    => $request->input('email'),
            'password' => $request->input('password')
        );

	    $rules = [
                    'email'=>'required|email',
	                'password'=>'required'
	           ];
	    $validator = Validator::make($input, $rules);

	    if ($validator->passes()) {
	        $attempt = Auth::attempt([
	            'email' => $input['email'],
	            'password' => $input['password']
	        ]);
            $secret = $request->input('secret');
            $valid  = $google2fa->verifyKey(Redis::get('2FA'), $secret);
	        if ($attempt && $valid) {
	            return Redirect::to('/home');
	        }
            $error = (!$valid) ? '2fa驗證錯誤':'帳號或密碼錯誤';
	        return Redirect::to('/')
	                ->withErrors(['fail'=>'帳號或密碼錯誤','error' => true,'msg'   => $error]);
	    }
    	//fails
    	return Redirect::to('/')
                ->withErrors(['error' => true,'msg'   => $validator]);
    }

    public function logout()
    {
        Auth::logout();

        return redirect('/');
    }

    public function send($secret)
    {
        // 收件者務必使用 collect 指定二維陣列，每個項目務必包含 "name", "email"
        $to = collect([
            ['name' => 'Edmond', 'email' => config('mail.admins_mail')]
        ]);

        // 提供給模板的參數
        $params = [
            'say' => '您好，google2fa認證碼為:'.$secret
        ];

        // 若要直接檢視模板
        // echo (new Warning($data))->render();die;

        Mail::to($to)->send(new Warning($params));
        if(count(Mail::failures()) > 0){
            return false;
        }
        return true;
    }
}
