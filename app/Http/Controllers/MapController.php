<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MapController extends Controller
{
	public function __construct(){
       parent::__construct();
    }
    public function index(){
    	return view('map');
    }
}
