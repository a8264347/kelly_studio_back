<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\View;
use App\User;
use App\Message;
use App\Contact;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct() {
    	$user = isset(auth()->user()->id)?auth()->user()->id:0;
    	$user_speaker_id=User::where('id', '=', $user)->select('speaker_id')->first();
    	$messages_not = 0;
		$messages     = array();
		$contact_not  = 0;
		$contact_arr  = array();
    	if($user_speaker_id){
    		$user_speaker_id = $user_speaker_id->toArray();
    		$search_not = [
			    ['parent_id', '=', $user_speaker_id['speaker_id']],
			    ['status', '=', 0]
			];
			$messages_not          = Message::where($search_not)->get()->count();
			$get_parent_to_speak   = User::where('id', '=', $user)->select('speaker_id')->first()->toArray();
			$get_speaker           = Speaker::where('id', '!=', $get_parent_to_speak['speaker_id'])->get()->toArray();
			$messages              = array();
			$speaker_arr           = array(0);

			foreach ($get_speaker as $key => $value) {
				$speaker_arr[] = $value['id'];
				$messages[$value['id']]['user'] = $value['name'];
				$messages[$value['id']]['uuid'] = $value['uuid'];
				$messages[$value['id']]['photo']= $value['photo'];
				$messages[$value['id']]['last_message']= '';
				$messages[$value['id']]['updated_at']  = '';
		        $messages[$value['id']]['unread']      = '';
			}
			$get_detail_message    = Message::whereIn('speaker_id', $speaker_arr)->orWhereIn('parent_id',$speaker_arr)->orderBy('created_at', 'desc')->take(1)->get()->toArray();
			foreach ($get_detail_message as $key => $value) {
				if (isset($messages[$value['speaker_id']]['uuid'])) {
		            $messages[$value['speaker_id']]['unread'] = empty($value['status'])?'unread':'';
					$messages[$value['speaker_id']]['last_message'] = $value['content'];
					$messages[$value['speaker_id']]['updated_at']   = date("Y-m-d H:i:s" , strtotime($value['updated_at']));
				}
			}
			$contact   = Contact::where('status', '=', 0)->get();
			$contact_not = $contact->count();
			$contact_arr = $contact->toArray();
			if (!empty($contact_arr)) {
				$zmdi_arr = array('zmdi-email-open','zmdi-account-box','zmdi-file-text','zmdi-notifications','zmdi-mood','zmdi-cloud-circle','zmdi-copy','zmdi-redo','zmdi-comment-list','zmdi-dot-circle');
				$zmdi = array_rand($zmdi_arr, 1);
				foreach ($contact_arr as $key => $value) {
					$contact_arr[$key]['zmdi']  = $zmdi[0];
					$contact_arr[$key]['email'] = $value['email'];
					$contact_arr[$key]['created_at'] = date("Y-m-d H:i:s" , strtotime($value['created_at']));
				}
			}
    	}
    	View::share ( 'messages_not', $messages_not );
        View::share ( 'messages', $messages );
        View::share ( 'contact_not', $contact_not );
        View::share ( 'contact_arr', $contact_arr );
    }
}
