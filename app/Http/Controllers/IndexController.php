<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use App\Customer;
use App\Contact;
use App\User;
use App\Message;
use App\Speaker;
use Validator;

class IndexController extends Controller
{
    public function __construct(){
       parent::__construct();
    }
	public function index(Request $request)
    {
    	$first = date('Y-m-01 00:00:00', strtotime(date("Y-m-d")));
		$last  = date('Y-m-t 23:59:59', strtotime('now'));

    	$data = array();
    	$customers = Customer::all()->count();
    	$data['contact_month'] = Contact::where('status', '=', 0)
    										->whereBetween('created_at', [$first, $last])
    										->get()->count();
    	$data['customers']     = Customer::all()->count();
    	$data['today']         = date("Y-m-d");
    	$users_arr			   = User::all()->toArray();
    	foreach ($users_arr as $key => $value) {
    		$users_arr[$key]['id'] = Crypt::encrypt($value['id']);
    		$users_arr[$key]['updated_at'] = date("Y-m-d",strtotime($value['updated_at']));
    	}
    	$data['users_arr']     = $users_arr;
        $user_speaker_id=User::where('id', '=', auth()->user()->id)->select('speaker_id')->first()->toArray();
    	$search_not = [
		    ['parent_id', '=', $user_speaker_id['speaker_id']],
		    ['status', '=', 0]
		];
		$data['messages_not']  = Message::where($search_not)->get()->count();
		$user 				   = auth()->user()->id;
		$get_parent_to_speak   = User::where('id', '=', $user)->select('speaker_id')->first()->toArray();
		$get_speaker           = Speaker::where('id', '!=', $get_parent_to_speak['speaker_id'])->get()->toArray();
		$messages              = array();
		$speaker_arr           = array(0);

		foreach ($get_speaker as $key => $value) {
			$speaker_arr[] = $value['id'];
			$messages[$value['id']]['user'] = $value['name'];
			$messages[$value['id']]['uuid'] = $value['uuid'];
			$messages[$value['id']]['photo']= $value['photo'];
			$messages[$value['id']]['last_message']= '';
			$messages[$value['id']]['updated_at']  = '';
            $messages[$value['id']]['unread']      = '';
		}
		$get_detail_message    = Message::whereIn('speaker_id', $speaker_arr)->orWhereIn('parent_id',$speaker_arr)->orderBy('created_at', 'desc')->take(1)->get()->toArray();
		foreach ($get_detail_message as $key => $value) {
			if (isset($messages[$value['speaker_id']]['uuid'])) {
                $messages[$value['speaker_id']]['unread'] = empty($value['status'])?'unread':'';
				$messages[$value['speaker_id']]['last_message'] = $value['content'];
				$messages[$value['speaker_id']]['updated_at']   = date("Y-m-d H:i:s" , strtotime($value['updated_at']));
			}
		}
		$data['messages']      = $messages;
        return view('home',$data);
    }
    public function getUser(){
    	$return = array(
    		'error' => 0,
    		'select'  => array()
    	);
    	$get_speaker = Speaker::all()->toArray();
    	foreach ($get_speaker as $key => $value) {
    		$return['select'][$key]['option_name'] = $value['name'];
    		$return['select'][$key]['uuid']        = $value['uuid'];
    	}
    	return json_encode($return);
    }
    public function getUserEdit(string $accessCode = ''){
    	$return = array(
    		'error' => 0,
    		'select'  => array()
    	);
    	$choice_speaker = 0;
    	if (!empty($accessCode)) {
    		$decrypted = Crypt::decrypt($accessCode);
    		$get_email = User::where('id', '=', $decrypted)->select(array('email','speaker_id'))->first()->toArray();
    		$choice_speaker  = $get_email['speaker_id'];
    		$return['email'] = $get_email['email'];
    	}
    	$get_speaker = Speaker::all()->toArray();
    	foreach ($get_speaker as $key => $value) {
    		$return['select'][$key]['selected']    = $choice_speaker == $value['id']?'selected':'';
    		$return['select'][$key]['option_name'] = $value['name'];
    		$return['select'][$key]['uuid']        = $value['uuid'];
    	}

    	return json_encode($return);
    }
    public function insertUser(Request $request){
    	$input      = $request->all();
    	$password   = $request->input('password', 0);
    	$password1  = $request->input('password1', 0);
    	$speaker_id = $request->input('speaker_id', 0);
    	if ($password != $password1) {
    		$return['error'] = 1;
			$return['msg']   = '新增使用者錯誤!兩者密碼不同。';
    	}
		$columns    = Speaker::where('uuid', '=', $speaker_id)->get()->toArray();
		if (!empty($columns)) {
			foreach ($columns as $columns_key => $columns_value) {
				$pass = Hash::make($password);
				$request->merge( array( 'speaker_id' => $columns_value['id'] ) );
				$request->merge( array( 'password'   => $pass ) );
				$input['password']   = $pass;
				$input['speaker_id'] = $columns_value['id'];
			}
		}
		$rules  = [
            'email'=>'required|email',
            'password' =>'required',
            'speaker_id' => 'nullable|integer|min:0|max:20',
        ];
        $access    = array('email', 'password','speaker_id');
	    $validator = Validator::make($input, $rules);
	    if ($validator->passes()) {
	    	$data    = $request->only($access);
	    	$users   = User::create($data);
	    	$return['msg'] = $users;
		}
		$return['error'] = 1;
		$return['msg']   = '新增使用者錯誤!'.$validator->errors();
        return json_encode($return);
    }
    public function updateUser(Request $request,string $accessCode = ''){
    	$input      = $request->all();
    	$speaker_id = $request->input('speaker_id', 0);
    	$password   = $request->input('password', 0);
    	$password1  = $request->input('password1', 0);
    	if ($password != $password1) {
    		$return['error'] = 1;
			$return['msg']   = '新增使用者錯誤!兩者密碼不同。';
    	}
		$columns    = Speaker::where('uuid', '=', $speaker_id)->get()->toArray();
		if (!empty($columns)) {
			foreach ($columns as $columns_key => $columns_value) {
				$pass = Hash::make($password);
				$request->merge( array( 'speaker_id' => $columns_value['id'] ) );
				$request->merge( array( 'password'   => $pass ) );
				$input['password']   = $pass;
				$input['speaker_id'] = $columns_value['id'];
			}
		}
    	$decrypted  = Crypt::decrypt($accessCode);
    	$user_update= User::where('id', '=', $decrypted)->firstOrFail();
		$rules  = [
            'email'=>'required|email',
            'password' =>'required',
            'speaker_id' => 'nullable|integer|min:0|max:20',
        ];
        $access = array('email', 'password','speaker_id');
	    $validator = Validator::make($input, $rules);
	    if ($validator->passes()) {
	    	$data    = $request->only($access);
			$users   = $user_update->update($data);
	    	$return['msg'] = $users;
		}
		$return['error'] = 1;
		$return['msg']   = '更新使用者錯誤!'.$validator->errors();
        return json_encode($return);
    }
    public function delUser(string $accessCode = ''){
    	$return = array(
    		'error'=> 0,
    		'msg'  => ''
    	);
    	$user_count = User::all()->count();
    	$decrypted  = Crypt::decrypt($accessCode);
    	if (is_numeric($decrypted) && $user_count > 1) {
    		$columns   = User::where('id', '=', $decrypted)->delete();
    	}
    	else{
    		$return['error'] = 1;
    		$return['msg']   = '錯誤code或最後一筆不得刪除!';
    	}
    	return json_encode($return);
    }
    public function getMassegeView(string $accessCode = ''){
        $return = array(
            'error' => 0,
            'msg'   => ''
        );
        $columns = Speaker::where('uuid', '=', $accessCode)->get()->toArray();
        $speaker_id = 0;
        if (!empty($columns)) {
            foreach ($columns as $key => $value) {
                $return['name']  = $value['name'];
                $return['photo'] = url($value['photo']);
                $speaker_id      = $value['id'];
            }
            //更新所有狀態為已讀
            Message::whereIn('speaker_id', array($speaker_id))->update(array('status' => 1));

            $get_detail_message  = Message::whereIn('speaker_id', array($speaker_id))->orWhereIn('parent_id',array($speaker_id))->orderBy('created_at', 'asc')->get()->toArray();
            if (!empty($get_detail_message)) {
                foreach ($get_detail_message as $key => $value) {
                    if ($value['parent_id'] == $speaker_id) {
                        $return['detail'][$key]['created_at']  = date("Y-m-d H:i:s",strtotime($value['created_at']));
                        $return['detail'][$key]['recei'] = $value['content'];
                    }
                    else{
                        $return['detail'][$key]['created_at']  = date("Y-m-d H:i:s",strtotime($value['created_at']));
                        $return['detail'][$key]['send']  = $value['content'];
                    }
                }
            }
        }
        return $return;
    }
    public function sendMassege(Request $request,string $uuid = ''){
        $return = array(
            'error' => 0,
            'msg'   => ''
        );
        $input      = $request->all();
        $content    = $request->input('content', '');
        $speakers = Speaker::where('uuid', '=', $uuid)->first()->toArray();
        if (!empty($content)) {
            $speaker_id = $speakers['id'];
            $parent_id  = auth()->user()->id;
            $user_speaker_id=User::where('id', '=', $parent_id)->select('speaker_id')->first()->toArray();
            $status     = 0;
            $request->merge( array( 'speaker_id' => $speaker_id ));
            $request->merge( array( 'parent_id'  => $user_speaker_id['speaker_id'] ));
            $request->merge( array( 'status'  => $status ));
            $access    = array('content','status','parent_id','speaker_id');
            $data      = $request->only($access);
            $data['speaker_id'] = $speaker_id;
            $data['parent_id']  = $user_speaker_id['speaker_id'];
            $data['status']     = $status;
            $message   = Message::create($data);
            $return['msg'] = $message;
            return json_encode($return);
        }
        $return['error'] = 1;
        $return['msg']   = '訊息新增失敗!';
        return json_encode($return);
    }
}
