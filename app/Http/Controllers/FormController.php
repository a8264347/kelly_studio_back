<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Speaker;
use App\Article;
use App\Blog;
use App\Customer;
use App\Contact;
use App\Slogan;
use App\Classes;
use Validator;
use App\Http\Requests\SpeakerRequest;
use App\Http\Requests\SloganRequest;
use App\Http\Requests\CustomerRequest;
use App\Http\Requests\ClassesRequest;
use App\Http\Requests\BlogRequest;
use App\Http\Requests\ArticleRequest;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;
use Storage;

class FormController extends Controller
{
	public function __construct(){
       parent::__construct();
    }
	public static $compare_config = array(
		'speakers'  => '講師',
		'articles'  => '文章',
		'blogs'     => '部落格',
		'customers' => '學員',
		'slogans'   => '標語',
		'classes'   => '課程'
	);
	public static $job_title_config = array(
		'job_title' => array(
			0 => '講師',
			1 => '外聘講師',
			2 => '助理',
			3 => '執行長'
		),
		'job_title_e' =>   array(
			0 => 'Lecturer',
			1 => 'Visiting Lecturer',
			2 => 'assistant',
			3 => 'ceo'
		)
	);
	public static $form_show = array(
		'name'        =>   array(
			'field'   => 'input',
			'lang'	  => '名稱'
		),
		'name_e'      =>   array(
			'field'   => 'input',
			'lang'	  => '英文名稱'
		),
		'title'       =>   array(
			'field'   => 'input',
			'lang'	  => '名稱'
		),
		'title_e'     =>   array(
			'field'   => 'input',
			'lang'	  => '英文名稱'
		),
		'job_title'   =>   array(
			'field'   => 'select',
			'lang'	  => '職稱'
		),
		'job_title_e' =>   array(
			'field'   => 'select',
			'lang'	  => '英文職稱'
		),
		'speakers_type'=>   array(
			'field'   => 'radios',
			'lang'	  => '類型'
		),
		'photo'       =>   array(
			'field'   => 'file',
			'lang'	  => '相片'
		),
		'type'        =>   array(
			'field'   => 'radios',
			'lang'	  => '狀態'
		),
		'introduction'=>   array(
			'field'   => 'textarea',
			'lang'	  => '介紹'
		),
		'introduction_e' =>   array(
			'field'   => 'textarea',
			'lang'	  => '英文介紹'
		),
		'content'     =>   array(
			'field'   => 'textarea',
			'lang'	  => '內容'
		),
		'content_e'   =>   array(
			'field'   => 'textarea',
			'lang'	  => '英文內容'
		),
		'link_fb'     =>   array(
			'field'   => 'textarea',
			'lang'	  => 'FB'
		),
		'created_by'  =>   array(
			'field'   => 'static',
			'lang'	  => '創建者'
		),
		'message'     =>   array(
			'field'   => 'textarea',
			'lang'	  => '訊息'
		),
		'class_id'    =>   array(
			'field'   => 'select',
			'lang'	  => '課程'
		),
		'classes_type'=>   array(
			'field'   => 'radios',
			'lang'	  => '狀態'
		),
		'price'       =>   array(
			'field'   => 'input',
			'lang'	  => '價錢'
		),
		'speaker_id'  =>   array(
			'field'   => 'select',
			'lang'	  => '講師'
		)
	);
	public static $contact_status = array(
		0 => array(
			'status' => '未核可',
			'color'  => 'admin'
		),
		1 => array(
			'status' => '已核可',
			'color'  => 'member'
		)
	);
	public static $customer_form_show = array(
		'name'        =>   array(
			'field'   => 'input',
			'lang'	  => '名稱'
		),
		'name_e'      =>   array(
			'field'   => 'input',
			'lang'	  => '英文名稱'
		),
		'job_title'   =>   array(
			'field'   => 'input',
			'lang'	  => '職稱'
		),
		'job_title_e' =>   array(
			'field'   => 'input',
			'lang'	  => '英文職稱'
		),
		'introduction'=>   array(
			'field'   => 'textarea',
			'lang'	  => '介紹'
		),
		'introduction_e' =>   array(
			'field'   => 'textarea',
			'lang'	  => '英文介紹'
		),
		'photo'       =>   array(
			'field'   => 'file',
			'lang'	  => '相片'
		),
		'message'     =>   array(
			'field'   => 'textarea',
			'lang'	  => '訊息'
		),
		'class_id'    =>   array(
			'field'   => 'select',
			'lang'	  => '課程'
		)
	);
    public function index(){
    	$contact   = Contact::where('status', '=', 0)->get()->toArray();
    	$speaker   = Speaker::all()->toArray();
    	$customers = Customer::orderBy('created_at','desc')->get()->toArray();
    	$articles  = Article::orderBy('created_at','desc')->get()->toArray();
    	$classes   = Classes::orderBy('created_at','desc')->get()->toArray();
    	$slogans   = Slogan::orderBy('created_at','desc')->get()->toArray();
    	$class_tmp = array();
    	if (!empty($classes)) {
    		foreach ($classes as $key => $value) {
    			$class_tmp[$value['id']] = $value['name'];
    		}
    	}
    	if (!empty($speaker)) {
    		foreach ($speaker as $key => $value) {
	    		foreach ($value as $value_key => $value_value) {
	    			if (in_array($value_key, array('job_title','job_title_e','speakers_type'))) {
	    				$stmt_key  = $value_key == 'speakers_type'?'job_title':'job_title';
	    				$speaker[$key][$value_key] = FormController::$job_title_config[$stmt_key][$value_value];
	    			}
	    			if ($value_key == 'created_at') {
	    				$speaker[$key]['created_at'] = date('Y-m-d H:i:s',strtotime($value_value));
	    			}
	    		}
	    	}
    	}
    	if (!empty($customers)) {
    		foreach ($customers as $key => $value) {
	    		foreach ($value as $value_key => $value_value) {
	    			if (in_array($value_key, array('class_id'))) {
	    				$class_name = isset($class_tmp[$value_value])?$class_tmp[$value_value]:'';
	    				$customers[$key][$value_key] = isset($class_name)&& !empty($class_name)?$class_name:'無';
	    			}
	    			if ($value_key == 'created_at') {
	    				$customers[$key]['created_at'] = date('Y-m-d H:i:s',strtotime($value_value));
	    			}
	    		}
	    	}
    	}
    	if (!empty($articles)) {
    		foreach ($articles as $key => $value) {
	    		foreach ($value as $value_key => $value_value) {
	    			if (in_array($value_key, array('type'))) {
	    				$articles[$key][$value_key]   = $value_value == 2 ? '關':'開';
	    				$articles[$key]['type_color'] = $value_value == 2 ? 'admin':'user';
	    			}
	    			if ($value_key == 'created_at') {
	    				$articles[$key]['created_at'] = date('Y-m-d H:i:s',strtotime($value_value));
	    			}
	    		}
	    	}
    	}
    	if (!empty($classes)) {
    		foreach ($classes as $key => $value) {
	    		foreach ($value as $value_key => $value_value) {
	    			if (in_array($value_key, array('classes_type'))) {
	    				$classes[$key][$value_key] = $value_value == 0 ? '關閉':'啟用';
	    			}
	    			if ($value_key == 'created_at') {
	    				$classes[$key]['created_at'] = date('Y-m-d H:i:s',strtotime($value_value));
	    			}
	    		}
	    	}
    	}
    	if (!empty($slogans)) {
    		foreach ($slogans as $key => $value) {
    			foreach ($value as $value_key => $value_value) {
    				if ($value_key == 'created_at') {
    					$slogans[$key]['created_at'] = date('Y-m-d',strtotime($value_value));
    				}
    			}
    		}
    	}
    	if ($contact) {
    		$status_color = FormController::$contact_status;
    		foreach ($contact as $key => $value) {
    			$class_name = isset($class_tmp[$value['class_id']])?$class_tmp[$value['class_id']]:'';
    			$contact[$key]['class']        = $class_name;
    			$contact[$key]['status']       = $status_color[$value['status']]['status'];
    			$contact[$key]['status_color'] = $status_color[$value['status']]['color'];
    			$contact[$key]['created_at']   = date('Y-m-d',strtotime($value['created_at']));
    		}
    	}
		$data['speakers'] = $speaker;
		$data['articles'] = $articles;
		$data['blogs'] = Blog::orderBy('created_at','desc')->get();
		$data['customers'] = $customers;
		$data['contacts']  = $contact;
		$data['slogans']   = $slogans;
		$data['classes']   = $classes;
		return view('main_form',$data);
    }
    public function getInsertForm(string $model){
    	$return  = array();
    	$columns = array();
    	$get_allow_key = array_keys(FormController::$form_show);
    	$get_customers_allow_key = array_keys(FormController::$customer_form_show);
    	switch ($model) {
    		case 'speakers':
    			$columns = Speaker::getTableColumns();
    			break;
    		case 'articles':
    			$columns = Article::getTableColumns();
    			break;
    		case 'blogs':
    			$columns = Blog::getTableColumns();
    			break;
    		case 'customers':
    			$columns = Customer::getTableColumns();
    			break;
    		case 'slogans':
    			$columns = Slogan::getTableColumns();
    			break;
    		case 'classes':
    			$columns = Classes::getTableColumns();
    			break;
    		default:
    			$return['error'] = 1;
    			$return['msg'] = '讀取欄位錯誤!';
    			return $return;
    			break;
    	}
    	foreach ($columns as $key => $value) {
    		if (in_array($value, $get_allow_key) && $model != 'customers') {
    			$return['data_view'][$value] = FormController::$form_show[$value]['field'];
    			$return['lang'][$value] = FormController::$compare_config[$model].FormController::$form_show[$value]['lang'];
    			if (FormController::$form_show[$value]['field'] === 'select' && in_array($value, array('job_title','job_title_e'))) {
    				$title_arr = FormController::$job_title_config[$value];
    				foreach ($title_arr as $arr_key => $arr_value) {
    					$return['select'][$value]['uuid'][] = $arr_key;
    					$return['select'][$value]['name'][] = $arr_value;
    				}
    			}
    			elseif (FormController::$form_show[$value]['field'] === 'select') {
    				$default_arr = array(
    					'uuid' => '',
    					'name' => ''
    				);
    				switch ($value) {
    					case 'class_id':
    						$class_item = Classes::orderBy('id', 'asc')
						                    ->groupBy('name')->get()->toArray();
							$return['select'][$value] = empty($class_item)?$default_arr:$class_item;
    						break;
    					case 'speaker_id':
    						$speaker_item = Speaker::orderBy('id', 'asc')
						                    ->groupBy('name')->get()->toArray();
							$return['select'][$value] = empty($speaker_item)?$default_arr:$speaker_item;
    						break;
    					default:
    						$return['select'][$value] = $default_arr;
    						break;
    				}
    			}
    		}
    		elseif (in_array($value, $get_customers_allow_key)) {
    			$return['data_view'][$value] = FormController::$customer_form_show[$value]['field'];
    			$return['lang'][$value] = FormController::$compare_config[$model].FormController::$customer_form_show[$value]['lang'];
    			if (FormController::$customer_form_show[$value]['field'] === 'select') {
    				$default_arr = array(
    					'uuid' => '',
    					'name' => ''
    				);
    				switch ($value) {
    					case 'class_id':
    						$class_item = Classes::orderBy('id', 'asc')
						                    ->groupBy('name')->get()->toArray();
							$return['select'][$value] = empty($class_item)?$default_arr:$class_item;
    						break;
    					case 'speaker_id':
    						$speaker_item = Speaker::orderBy('id', 'asc')
						                    ->groupBy('name')->get()->toArray();
							$return['select'][$value] = empty($speaker_item)?$default_arr:$speaker_item;
    						break;
    					default:
    						$return['select'][$value] = $default_arr;
    						break;
    				}
    			}
    		}
    	}
    	$return['form_name'] = isset(FormController::$compare_config[$model])?'新增'.FormController::$compare_config[$model]:'';
    	return $return;
    }
    public function getEditForm(string $model,string $uuid){
    	$return  = array();
    	$columns = array();
    	$get_allow_key = array_keys(FormController::$form_show);
    	$get_customers_allow_key = array_keys(FormController::$customer_form_show);
    	switch ($model) {
    		case 'speakers':
    			$columns = Speaker::where('uuid', '=', $uuid)->firstOrFail()->toArray();
    			break;
    		case 'articles':
    			$columns = Article::where('uuid', '=', $uuid)->firstOrFail()->toArray();
    			break;
    		case 'blogs':
    			$columns = Blog::where('uuid', '=', $uuid)->firstOrFail()->toArray();
    			break;
    		case 'customers':
    			$columns = Customer::where('uuid', '=', $uuid)->firstOrFail()->toArray();
    			break;
    		case 'slogans':
    			$columns = Slogan::where('uuid', '=', $uuid)->firstOrFail()->toArray();
    			break;
    		case 'classes':
    			$columns = Classes::where('uuid', '=', $uuid)->firstOrFail()->toArray();
    			break;
    		default:
    			$return['error'] = 1;
    			$return['msg'] = '讀取資料錯誤!';
    			break;
    	}
    	if (!empty($columns)) {
    		foreach ($columns as $key => $value) {
    			if (in_array($key, $get_allow_key) && $model != 'customers') {
	    			$return['data_view'][$key] = FormController::$form_show[$key]['field'];
	    			$return['lang'][$key] = FormController::$compare_config[$model].FormController::$form_show[$key]['lang'];
	    			if ($key == 'photo' && !empty($value)) {
	    				$stmt_value = explode('-', $value);
	    				$value = end($stmt_value);
	    			}
	    			$return['value'][$key] = $value;
	    			if (FormController::$form_show[$key]['field'] === 'select' && in_array($key, array('job_title','job_title_e'))) {
	    				$title_arr = FormController::$job_title_config[$key];
	    				foreach ($title_arr as $arr_key => $arr_value) {
	    					$return['select'][$key]['uuid'][] = $arr_key;
	    					$return['select'][$key]['name'][] = $arr_value;
	    					$return['select'][$key]['value'][]= $value;
	    				}
	    			}
	    			elseif (FormController::$form_show[$key]['field'] === 'select') {
	    				$default_arr = array(
	    					'uuid' 		=> '',
	    					'name' 		=> '',
	    					'selected' => ''
	    				);
	    				switch ($key) {
	    					case 'class_id':
	    						$class_item = Classes::orderBy('id', 'asc')
							                    ->groupBy('name')
							                    ->having('classes_type', '>', 0)
							                    ->get()->toArray();
							    if (!empty($class_item)) {
							    	foreach ($class_item as $item_key => $item_value) {
							    		$class_item[$item_key]['selected'] = $value == $value['id']?'selected':'';
							    	}
							    }
								$return['select'][$key] = empty($class_item)?$default_arr:$class_item;
	    						break;
	    					case 'speaker_id':
	    						$speaker_item = Speaker::orderBy('id', 'asc')
							                    ->groupBy('name')
							                    ->having('speakers_type', '>', 0)
							                    ->get()->toArray();
							    if (!empty($speaker_item)) {
							    	foreach ($speaker_item as $item_key => $item_value) {
							    		$speaker_item[$item_key]['selected'] = $value == $value['id']?'selected':'';
							    	}
							    }
								$return['select'][$key] = empty($speaker_item)?$default_arr:$speaker_item;
	    						break;
	    					default:
	    						$return['select'][$key] = $default_arr;
	    						break;
	    				}
	    			}
	    		}
	    		elseif (in_array($key, $get_customers_allow_key)) {
	    			$return['data_view'][$key] = FormController::$customer_form_show[$key]['field'];
	    			$return['lang'][$key] = FormController::$compare_config[$model].FormController::$customer_form_show[$key]['lang'];
	    			if ($key == 'photo' && !empty($value)) {
	    				$stmt_value = explode('-', $value);
	    				$value = end($stmt_value);
	    			}
	    			$return['value'][$key] = $value;
	    			if (FormController::$customer_form_show[$key]['field'] === 'select') {
	    				$default_arr = array(
	    					'uuid' 		=> '',
	    					'name' 		=> '',
	    					'selected' => ''
	    				);
	    				switch ($key) {
	    					case 'class_id':
	    						$class_item = Classes::orderBy('id', 'asc')
							                    ->groupBy('name')
							                    ->having('classes_type', '>', 0)
							                    ->get()->toArray();
							    if (!empty($class_item)) {
							    	foreach ($class_item as $item_key => $item_value) {
							    		$class_item[$item_key]['selected'] = $value == $value['id']?'selected':'';
							    	}
							    }
								$return['select'][$key] = empty($class_item)?$default_arr:$class_item;
	    						break;
	    					case 'speaker_id':
	    						$speaker_item = Speaker::orderBy('id', 'asc')
							                    ->groupBy('name')
							                    ->having('speakers_type', '>', 0)
							                    ->get()->toArray();
							    if (!empty($speaker_item)) {
							    	foreach ($speaker_item as $item_key => $item_value) {
							    		$speaker_item[$item_key]['selected'] = $value == $value['id']?'selected':'';
							    	}
							    }
								$return['select'][$key] = empty($speaker_item)?$default_arr:$speaker_item;
	    						break;
	    					default:
	    						$return['select'][$key] = $default_arr;
	    						break;
	    				}
	    			}
	    		}
	    	}
    	}
    	$return['form_name'] = isset(FormController::$compare_config[$model])?'編輯'.FormController::$compare_config[$model]:'';
    	return $return;
    }
    public function updateData(Request $request,string $model,string $uuid)
    {
    	$input = $request->all();
    	if ($request->hasFile('photo')) {
    		$url_return = $this->savePhoto($request->file('photo'));
        }
    	switch ($model) {
    		case 'speakers':
    			$speaker_update = Speaker::where('uuid', '=', $uuid)->firstOrFail();
    			$speak     = new SpeakerRequest();
    			$rules     = $speak->rules();
			    $validator = Validator::make($input, $rules);
			    if ($validator->passes()) {
			    	$access  = Speaker::getAccessField();
			    	$data    = $request->only($access);
    				if (isset($url_return)) {
			    		$data['photo'] = $url_return;
			    	}
    				$data['updated_by'] = auth()->user()->id;
    				$speaker = $speaker_update->update($data);
			    	$return['msg'] = $speaker;
			    	break;
				}
				$return['error'] = 1;
				$return['msg']   = '新增講師資料錯誤!'.$validator->errors();
    			break;
    		case 'articles':
    			$articles_update = Article::where('uuid', '=', $uuid)->firstOrFail();
    			$article   = new ArticleRequest();
    			$rules     = $article->rules();
			    $validator = Validator::make($input, $rules);
			    if ($validator->passes()) {
    				$access  = Article::getAccessField();
			    	$data    = $request->only($access);
			    	if (isset($url_return)) {
			    		$data['photo'] = $url_return;
			    	}
    				$data['updated_by'] = auth()->user()->id;
			    	$article = $articles_update->update($data);
			    	$return['msg'] = $article;
			    	break;
				}
				$return['error'] = 1;
				$return['msg']   = '新增文章資料錯誤!'.$validator->errors();
    			break;
    		case 'blogs':
    			$blog_update = Blog::where('uuid', '=', $uuid)->firstOrFail();
    			$blog        = new BlogRequest();
    			$rules       = $blog->rules();
			    $validator   = Validator::make($input, $rules);
			    if ($validator->passes()) {
    				$access  = Blog::getAccessField();
			    	$data    = $request->only($access);
			    	if (isset($url_return)) {
			    		$data['photo'] = $url_return;
			    	}
    				$data['updated_by'] = auth()->user()->id;
    				$blog = $blog_update->update($data);
			    	$return['msg'] = $blog;
			    	break;
				}
				$return['error'] = 1;
				$return['msg']   = '新增文章資料錯誤!'.$validator->errors();
    			break;
    		case 'customers':
    			$customer_update = Customer::where('uuid', '=', $uuid)->firstOrFail();
    			$class_id  = $request->input('class_id', 0);
    			$columns   = Classes::where('uuid', '=', $class_id)->get()->toArray();
    			if (!empty($columns)) {
    				foreach ($columns as $columns_key => $columns_value) {
    					$request->merge( array( 'class_id' => $columns_value['id'] ) );
    					$input['class_id'] = $columns_value['id'];
    				}
    			}
    			$customer  = new CustomerRequest();
    			$rules     = $customer->rules();
			    $validator = Validator::make($input, $rules);
			    if ($validator->passes()) {
    				$access  = Customer::getAccessField();
			    	$data    = $request->only($access);
			    	if (isset($url_return)) {
			    		$data['photo'] = $url_return;
			    	}
    				$data['updated_by'] = auth()->user()->id;
			    	$customer = $customer_update->update($data);
			    	$return['msg'] = $customer;
			    	break;
				}
				$return['error'] = 1;
				$return['msg']   = '新增學員資料錯誤!'.$validator->errors();
    			break;
    		case 'slogans':
    			$slogan_update = Slogan::where('uuid', '=', $uuid)->firstOrFail();
    			$slogan        = new SloganRequest();
    			$rules     	   = $slogan->rules();
			    $validator 	   = Validator::make($input, $rules);
			    if ($validator->passes()) {
    				$access  = Slogan::getAccessField();
			    	$data    = $request->only($access);
    				$data['updated_by'] = auth()->user()->id;
    				$slogan = $slogan_update->update($data);
			    	$return['msg'] = $slogan;
			    	break;
				}
				$return['error'] = 1;
				$return['msg']   = '新增學員資料錯誤!'.$validator->errors();
    			break;
    		case 'classes':
    			$classes_update = Classes::where('uuid', '=', $uuid)->firstOrFail();
    			$speaker_id = $request->input('speaker_id', 0);
    			$columns    = Speaker::where('uuid', '=', $speaker_id)->get()->toArray();
    			if (!empty($columns)) {
    				foreach ($columns as $columns_key => $columns_value) {
    					$request->merge( array( 'speaker_id' => $columns_value['id'] ) );
    					$input['speaker_id'] = $columns_value['id'];
    				}
    			}
    			$classes    = new ClassesRequest();
    			$rules      = $classes->rules();
			    $validator  = Validator::make($input, $rules);
			    if ($validator->passes()) {
    				$access  = Classes::getAccessField();
			    	$data    = $request->only($access);
			    	if (isset($url_return)) {
			    		$data['photo'] = $url_return;
			    	}
    				$data['updated_by'] = auth()->user()->id;
			    	$classes = $classes_update->update($data);
			    	$return['msg'] = $classes;
			    	break;
				}
				$return['error'] = 1;
				$return['msg']   = '新增課程資料錯誤!'.$validator->errors();
    			break;
    		default:
    			$return['error'] = 1;
    			$return['msg'] = '讀取資料錯誤!';
    			break;
    	}
		return json_encode($return);
    }
    public function InsertForm(Request $request,string $model){
    	$return = array(
    		'error' => 0,
    		'msg'   => ''
    	);
    	$input = $request->all();
    	if ($request->hasFile('photo')) {
    		$url_return = $this->savePhoto($request->file('photo'));
        }
    	switch ($model) {
    		case 'speakers':
    			$speak     = new SpeakerRequest();
    			$rules     = $speak->rules();
			    $validator = Validator::make($input, $rules);
			    if ($validator->passes()) {
			    	$access  = Speaker::getAccessField();
			    	$data    = $request->only($access);
    				$data['photo'] = isset($url_return)?$url_return:'';
			    	$data['created_by'] = auth()->user()->id;
    				$data['updated_by'] = auth()->user()->id;
			    	$speaker = Speaker::create($data);
			    	$return['msg'] = $speaker;
			    	break;
				}
				$return['error'] = 1;
				$return['msg']   = '新增講師資料錯誤!'.$validator->errors();
    			break;
    		case 'articles':
    			$article   = new ArticleRequest();
    			$rules     = $article->rules();
			    $validator = Validator::make($input, $rules);
			    if ($validator->passes()) {
    				$access  = Article::getAccessField();
			    	$data    = $request->only($access);
			    	$data['photo'] = isset($url_return)?$url_return:'';
			    	$data['created_by'] = auth()->user()->id;
    				$data['updated_by'] = auth()->user()->id;
			    	$article = Article::create($data);
			    	$return['msg'] = $article;
			    	break;
				}
				$return['error'] = 1;
				$return['msg']   = '新增文章資料錯誤!'.$validator->errors();
    			break;
    		case 'blogs':
    			$blog      = new BlogRequest();
    			$rules     = $blog->rules();
			    $validator = Validator::make($input, $rules);
			    if ($validator->passes()) {
    				$access  = Blog::getAccessField();
			    	$data    = $request->only($access);
			    	$data['photo'] = isset($url_return)?$url_return:'';
			    	$data['created_by'] = auth()->user()->id;
    				$data['updated_by'] = auth()->user()->id;
			    	$blog = Blog::create($data);
			    	$return['msg'] = $blog;
			    	break;
				}
				$return['error'] = 1;
				$return['msg']   = '新增文章資料錯誤!'.$validator->errors();
    			break;
    		case 'customers':
    			$class_id  = $request->input('class_id', 0);
    			$columns   = Classes::where('uuid', '=', $class_id)->get()->toArray();
    			if (!empty($columns)) {
    				foreach ($columns as $columns_key => $columns_value) {
    					$request->merge( array( 'class_id' => $columns_value['id'] ) );
    					$input['class_id'] = $columns_value['id'];
    				}
    			}
    			$customer  = new CustomerRequest();
    			$rules     = $customer->rules();
			    $validator = Validator::make($input, $rules);
			    if ($validator->passes()) {
    				$access  = Customer::getAccessField();
			    	$data    = $request->only($access);
			    	$data['photo'] = isset($url_return)?$url_return:'';
			    	$data['created_by'] = auth()->user()->id;
    				$data['updated_by'] = auth()->user()->id;
			    	$customer = Customer::create($data);
			    	$return['msg'] = $customer;
			    	break;
				}
				$return['error'] = 1;
				$return['msg']   = '新增學員資料錯誤!'.$validator->errors();
    			break;
    		case 'slogans':
    			$slogan    = new SloganRequest();
    			$rules     = $slogan->rules();
			    $validator = Validator::make($input, $rules);
			    if ($validator->passes()) {
    				$access  = Slogan::getAccessField();
			    	$data    = $request->only($access);
			    	$data['created_by'] = auth()->user()->id;
    				$data['updated_by'] = auth()->user()->id;
			    	$slogan = Slogan::create($data);
			    	$return['msg'] = $slogan;
			    	break;
				}
				$return['error'] = 1;
				$return['msg']   = '新增學員資料錯誤!'.$validator->errors();
    			break;
    		case 'classes':
    			$speaker_id= $request->input('speaker_id', 0);
    			$columns   = Speaker::where('uuid', '=', $speaker_id)->get()->toArray();
    			if (!empty($columns)) {
    				foreach ($columns as $columns_key => $columns_value) {
    					$request->merge( array( 'speaker_id' => $columns_value['id'] ) );
    					$input['speaker_id'] = $columns_value['id'];
    				}
    			}
    			$classes   = new ClassesRequest();
    			$rules     = $classes->rules();
			    $validator = Validator::make($input, $rules);
			    if ($validator->passes()) {
    				$access  = Classes::getAccessField();
			    	$data    = $request->only($access);
			    	$data['photo'] = isset($url_return)?$url_return:'';
			    	$data['created_by'] = auth()->user()->id;
    				$data['updated_by'] = auth()->user()->id;
			    	$classes = Classes::create($data);
			    	$return['msg'] = $classes;
			    	break;
				}
				$return['error'] = 1;
				$return['msg']   = '新增課程資料錯誤!'.$validator->errors();
    			break;
    		default:
    			$return['error'] = 1;
    			$return['msg'] = '讀取資料錯誤!';
    			break;
    	}
		return json_encode($return);
    }
    public function delData(Request $request,string $model){
    	$return = array(
    		'error' => 0,
    		'msg'   => ''
    	);
    	$uuid = $request->input('uuid', 0);
    	if (!empty($uuid)) {
    		switch ($model) {
	    		case 'speakers':
	    			$columns = Speaker::where('uuid', '=', $uuid)->delete();
	    			break;
	    		case 'articles':
	    			$columns = Article::where('uuid', '=', $uuid)->delete();
	    			break;
	    		case 'blogs':
	    			$columns = Blog::where('uuid', '=', $uuid)->delete();
	    			break;
	    		case 'customers':
	    			$columns = Customer::where('uuid', '=', $uuid)->delete();
	    			break;
	    		case 'slogans':
	    			$columns = Slogan::where('uuid', '=', $uuid)->delete();
	    			break;
	    		case 'classes':
	    			$columns = Classes::where('uuid', '=', $uuid)->delete();
	    			break;
	    		default:
	    			$return['error'] = 1;
	    			$return['msg'] = '刪除資料錯誤!';
	    			break;
	    	}
    	}
    	return json_encode($return);
    }
    public function checkContact(Request $request){
    	$return = array(
    		'error' => 0,
    		'msg'   => ''
    	);
    	$uuid = $request->input('uuid', 0);
    	$contact = Contact::where('uuid', $uuid)->update(array('status' => 1));
    	if (!$contact) {
    		$return['error'] = 1;
    		$return['msg']   = '更新報名資訊失敗';
    	}
    	return json_encode($return);
    }
    public function savePhoto(UploadedFile $image){
    	$file_name = auth()->user()->id . '-'  . Str::random(8). '-' . $image->getClientOriginalName();
    	Storage::put($file_name, $image->get());
    	return 'storage/'.$file_name;
    }
}
