<?php

namespace App;

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Classes extends Model
{
    protected $table = 'classes';
    protected $fillable = [
        'name',
        'name_e',
        'classes_type',
        'price',
        'photo',
        'introduction',
        'introduction_e',
        'content',
        'content_e',
        'speaker_id',
        'created_by',
        'updated_by'
    ];
    protected $access = array(
        'name',
        'name_e',
        'classes_type',
        'price',
        'photo',
        'introduction',
        'introduction_e',
        'content',
        'content_e',
        'speaker_id'
    );
    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->uuid = Str::uuid();
        });
    }
    protected function getAccessField(){
        return $this->access;
    }
    public function getSchemaTable()
    {
        return $this->table;
    }
    protected function getTableColumns()
	{
	    return Schema::getColumnListing($this->getSchemaTable());
	}
}
