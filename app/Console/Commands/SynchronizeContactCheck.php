<?php

namespace App\Console\Commands;

use App\BotMessage;
use Illuminate\Console\Command;
use App\Contact;
use Storage;

class SynchronizeContactCheck extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'kellyback:syn-contact';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '同步 Contact 已確認的報名事項';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $first = date('Y-m-d 00:00:00', strtotime(strtotime("-1 day")));
        $last  = date('Y-m-d 23:59:59', strtotime('now'));

        $contact_day = Contact::where('status', '=', 1)
                                        ->whereBetween('created_at', [$first, $last])
                                        ->get()->toArray();
        if (!empty($contact_day)) {
            $uuid_arr = array();
            foreach ($contact_month as $key => $value) {
                $uuid_arr[] = $value['uuid'];
            }
            $txt = implode("-",$uuid_arr);
            Storage::put('Synchronize.txt', $txt);
        }
    }
}
