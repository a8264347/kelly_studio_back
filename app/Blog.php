<?php

namespace App;

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Blog extends Model
{
	protected $table = 'blogs';
    protected $fillable = [
        'title',
        'title_e',
        'short_introduction',
        'short_introduction_e',
        'content',
        'content_e',
        'photo',
        'created_by',
        'updated_by'
    ];
    protected $access = array(
        'title',
        'title_e',
        'short_introduction',
        'short_introduction_e',
        'content',
        'content_e',
        'photo'
    );
    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->uuid = Str::uuid();
        });
    }
    protected function getAccessField(){
        return $this->access;
    }
    public function getSchemaTable()
    {
        return $this->table;
    }
    protected function getTableColumns()
    {
        return Schema::getColumnListing($this->getSchemaTable());
    }
}
