<?php

namespace App;

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class ArticleDesc extends Model
{
	protected $primaryKey = null;
	public $incrementing  = false;
	protected $table  = 'article_desc';
    protected $fillable = [
        'article_id',
        'content',
        'content_e',
        'photo',
        'photo_desc',
        'type',
        'created_by',
        'updated_by'
    ];
    protected $access = array(
        'article_id',
        'content',
        'content_e',
        'photo',
        'photo_desc',
        'type'
    );
    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->uuid = Str::uuid();
        });
    }
    protected function getAccessField(){
        return $this->access;
    }
    public function getSchemaTable()
    {
        return $this->table;
    }
    protected function getTableColumns()
    {
        return Schema::getColumnListing($this->getSchemaTable());
    }
}
