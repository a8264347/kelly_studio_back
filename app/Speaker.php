<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class Speaker extends Model
{
	protected $table  = 'speakers';
    protected $fillable = [
        'name',
        'name_e',
        'job_title',
        'job_title_e',
        'speakers_type',
        'photo',
        'introduction',
        'introduction_e',
        'content',
        'content_e',
        'link_fb',
        'created_by',
        'updated_by'
    ];
    protected $access = array(
        'name',
        'name_e',
        'job_title',
        'job_title_e',
        'speakers_type',
        'photo',
        'introduction',
        'introduction_e',
        'content',
        'content_e',
        'link_fb'
    );
    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->uuid = Str::uuid();
        });
    }
    protected function getAccessField(){
        return $this->access;
    }
	public function getSchemaTable()
    {
        return $this->table;
    }
    protected function getTableColumns()
    {
        return Schema::getColumnListing($this->getSchemaTable());
    }
}
